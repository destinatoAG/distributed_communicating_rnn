#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 01:04:14 2021

@author: pabudu
"""

def fn1_2nodes(a,b):
    if (a.data.numpy() > 0.4 and b.data.numpy() < 0.6):
        t = 1.0
    else:
        t = 0.0   
    return t 

def fn2_2nodes(a,b):
   if ((a.data.numpy() < 0.3 and b.data.numpy() < 0.3) or (a.data.numpy() > 0.7 and b.data.numpy() > 0.7)):
        t = 1.0
   else:
        t = 0.0
   return t

def fn3_2nodes(a,b):
   if (a.data.numpy() > b.data.numpy()):
       t = 1.0
   else:
       t = 0.0
   return t


def fn_3nodes(a,b):
    if (a.data.numpy() > b.data.numpy()):
        return a
    return b

def fn1_3nodes(a,b,c):
    if (a.data.numpy() > 0.4 and b.data.numpy() < 0.6 and c.data.numpy() > 0.5):
        t = 1.0
    else:
        t = 0.0   
    return t 

def fn2_3nodes(a,b,c):
   if ((a.data.numpy() < 0.3 and b.data.numpy() < 0.3) or (a.data.numpy() > 0.7 and b.data.numpy() > 0.7) or (a.data.numpy() < 0.3 and c.data.numpy() < 0.3) or (a.data.numpy() > 0.7 and c.data.numpy() > 0.7) or (b.data.numpy() < 0.3 and c.data.numpy() < 0.3) or (b.data.numpy() > 0.7 and c.data.numpy() > 0.7)):
        t = 1.0
   else:
        t = 0.0
   return t

def fn3_3nodes(a,b,c):
   if (c.data.numpy() > b.data.numpy()) and (c.data.numpy() > a.data.numpy()):
       t = 1.0
   else:
       t = 0.0
   return t

