  # -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 22:35:02 2018

@author: priudu

"""

import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import matplotlib.pyplot as plt 
import torch.optim as optim
from objectives import * 

class Node(nn.Module):

    def __init__(self):
      
        #super(Node, self).__init__()
        super().__init__()
        # These are the weights (common) in the whole network
        self.fc1 = nn.Linear(5, 8)
        self.fc2 = nn.Linear(8, 8)
        self.fc3 = nn.Linear(8, 5)
        
     
    def step(self,state):
        """ This is an internal function which runs a single step/iteration of a node"""
        "tied weights here"
        state = F.relu(F.linear(state,self.fc1.weight,self.fc1.bias))
        state =  F.relu(F.linear(state,self.fc2.weight,self.fc2.bias))
        state =  F.linear(state,self.fc3.weight,self.fc3.bias)
        return state

    #input_seq in this case is 2, it takes directly from seq_len, its the number of timesteps really, or number of updates you want to the network really
    def forward(self, input_seq):
        """ This takes in an input sequence and uses it to run through the network"""
        # Iteration 0: hidden states are set to 0
        state_a = Variable(torch.zeros(5))
        state_b = Variable(torch.zeros(5))
        #state_c = Variable(torch.zeros(5))
        
        for k in range(len(input_seq)):
            # The first state is set ("forced") to be the input state - this is really equivalent to concatentation
            # State 0 is our sensor input channel
            state_a[0] = input_seq[0,0]
            state_b[0] = input_seq[0,1]
            #state_c[0] = input_seq[0,1]
            # Now we "communicate", ready for the next iteration
            # State 1 is our "input comms" channel
            # State 2 is our "output comms" channel 
            # NB: note the use of the clone operation to avoid issues with in-place modification
            # https://discuss.pytorch.org/t/encounter-the-runtimeerror-one-of-the-variables-needed-for-gradient-computation-has-been-modified-by-an-inplace-operation/836/3
            state_a[1] = state_b[2].clone()
            state_b[1] = state_a[2].clone()
            #state_c[1] = state_b[2].clone()
            
            state_a = self.step(state_a)
            state_b = self.step(state_b)
            #state_c = self.step(state_b)
        # the last element of the state is our "output"    
        return state_a[-1]

net = Node()

def count_parameters(net):
    return sum(p.numel() for p in net.parameters() if p.requires_grad)

params_trainable = count_parameters(net)

print('Parameters Number - Trainable: ', params_trainable)

num_neurons = 8

params_all = sum(p.numel() for p in net.parameters())

print('Parameters Number - ALL', params_all)
params = list(net.parameters())

layers_total = len(params)
print('Total Number of Layers', layers_total)

hidden_total = layers_total - 2

print('Hidden Layers', hidden_total)

params = list(net.parameters())

# Apply an input (sequence of length)
seq_length = 2
num_nodes = 2

criterion = nn.MSELoss()

# create your optimizer
optimizer = optim.Adam(net.parameters(), lr=0.0005)
 
a = np.asarray([])
b = np.asarray([])
#c = np.asarray([])
t_new = np.asarray([])
predictions = np.asarray([])

#In our training loop
losses = []
step_losses = []
timesteps = 2

from datetime import datetime
start=datetime.now()

running_loss = 0.0
for k in range(10000):
    new_input = Variable(torch.rand(2,num_nodes))
    
    input = new_input.data.numpy() 
    input = input[0]
    input = np.vstack((input, np.atleast_1d(input).T))
    input = torch.autograd.Variable(torch.from_numpy(input)).float()

    #Change function here
    target = fn1_2nodes(input[0][0],input[0][1])
    target = np.array([target])
    target = torch.autograd.Variable(torch.from_numpy(target)).float()
     
    optimizer.zero_grad()   # zero the gradient buffers
    output_states = net(input)
    output = output_states

    a = np.append(a, input[0][0]) 
    b = np.append(b, input[0][1])
    #c = np.append(c,input[0][2])
    t_new = np.append(t_new, target)
    
    predictions = np.append(predictions, output.data)  
        
    loss = criterion(output, target)

    loss.backward()
    optimizer.step()    # Does the updat
    
    losses.append(loss.data.numpy())
    
    running_loss += loss.data
    
    if k % 1000 == 999:    # print every 2000 mini-batches
        print('[%5d] loss: %.3f' %
              (k + 1, running_loss / 1000))
        loss_data = running_loss/1000
        step_losses.append(loss_data)
        running_loss = 0.0

time = datetime.now()-start
print ("Time to run program %s" %(time))

t_new = t_new.astype(int)

fig_11, ax8 = plt.subplots(figsize=(5,3))
plt.title('Target function')
ax8.scatter(a,b, s=100, c=t_new, cmap=plt.cm.autumn)
plt.show(ax8)

fig_12, ax8 = plt.subplots(figsize=(5,3))
plt.title('Target function')
ax8.scatter(a,b, s=100, c=t_new, cmap=plt.cm.winter)
plt.show(ax8)

fig_13, ax5 = plt.subplots(figsize=(5,3))
plt.title('RNN Output predictions')
ax5.scatter(a, b, s=100, c=predictions, cmap=plt.cm.autumn)
plt.show(ax5)

fig_14, ax5 = plt.subplots(figsize=(5,3))
plt.title('RNN Output predictions')
ax5.scatter(a, b, s=100, c=predictions, cmap=plt.cm.winter)
plt.show(ax5)

fig_15, ax6 = plt.subplots(figsize=(5,3))
plt.title('Step Losses')
plt.plot(step_losses)
plt.show(ax6)

fig_16, ax7 = plt.subplots(figsize=(5,3))
plt.title('All Losses')
plt.plot(losses)
plt.show(ax7)

file = open('result.txt', 'w')

file.writelines('Function: 3 - Compare    \n')
file.writelines('Architecture/States: ' + 'N/A' + '\n')
file.writelines('Number of Nodes: ' + repr(num_nodes) + '\n')
file.writelines('Sequence Length: ' + repr(seq_length) + '\n')
file.writelines('Number of Parameters - ALL: ' + repr(params_all) + '\n')
file.writelines('Number of Parameters - Trainable: ' + repr(params_trainable) + '\n')
file.writelines('Total Number of Layers: ' + repr(layers_total) + '\n')
file.writelines('Number of Hidden Layers: ' + repr(hidden_total) + '\n')
file.writelines('Number of Number of Hidden Neurons: ' + repr(num_neurons) + '\n')
file.writelines('Number of Timesteps: ' + repr(timesteps) + '\n')
file.writelines('Steps/Epochs: ' + repr(k+1) + '\n')
file.writelines('Step Losses: ' + repr(step_losses) + '\n') 
file.writelines('Time: ' + str(time) + '\n')
file.writelines('Lowest Loss, at ' + repr(k+1) + ' steps/epochs: ' + repr(step_losses[-1]) + '\n') 

file.close()





























   
   