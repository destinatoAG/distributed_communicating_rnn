# -*- coding: utf-8 -*-
"""
Created on Mon Jan 29 17:47:12 2018

@author: destinatoag
"""

from datetime import datetime  
import matplotlib.pyplot as plt
import numpy as np

f_10000 = ['0:01:25.327721', '0:01:18.237238', '0:01:18.533622', '0:01:15.189799']
f_15000 = ['0:02:12.816449', '0:02:01.244248', '0:02:00.800398', '0:02:03.615422']
f_25000 = ['0:04:19.165552', '0:03:52.450567', '0:03:50.814022', '0:04:04.418304']
f_50000 = ['0:10:32.610618', '0:10:36.020079', '0:10:15.493740', '0:12:09.810354']

f_10000 = [datetime.strptime(x, '%H:%M:%S.%f') for x in f_10000] 
f_15000 = [datetime.strptime(x, '%H:%M:%S.%f') for x in f_15000]
f_25000 = [datetime.strptime(x, '%H:%M:%S.%f') for x in f_25000]
f_50000 = [datetime.strptime(x, '%H:%M:%S.%f') for x in f_50000]

f_10000_1 = f_10000[0]
f_10000_2 = f_10000[1]
f_10000_3 = f_10000[2]

f_15000_1 = f_15000[0]
f_15000_2 = f_15000[1]
f_15000_3 = f_15000[2]

f_25000_1 = f_25000[0]
f_25000_2 = f_25000[1]
f_25000_3 = f_25000[2]

f_50000_1 = f_50000[0]
f_50000_2 = f_50000[1]
f_50000_3 = f_50000[2]

x = '0:00:00.000000'
x = datetime.strptime(x, '%H:%M:%S.%f')

f_10000_1 = (f_10000[0] - x).total_seconds()
f_10000_2 = (f_10000[1] - x).total_seconds()
f_10000_3 = (f_10000[2] - x).total_seconds()

f_15000_1 = (f_15000[0] - x).total_seconds()
f_15000_2 = (f_15000[1] - x).total_seconds()
f_15000_3 = (f_15000[2] - x).total_seconds()

f_25000_1 = (f_25000[0] - x).total_seconds()
f_25000_2 = (f_25000[1] - x).total_seconds()
f_25000_3 = (f_25000[2] - x).total_seconds()

f_50000_1 = (f_50000[0] - x).total_seconds()
f_50000_2 = (f_50000[1] - x).total_seconds()
f_50000_3 = (f_50000[2] - x).total_seconds()

f_10000 = [f_10000_1,f_10000_2, f_10000_3]
f_15000 = [f_15000_1,f_15000_2, f_15000_3]
f_25000 = [f_25000_1,f_25000_2, f_25000_3]
f_50000 = [f_50000_1,f_50000_2, f_50000_3]


f_0 = np.array([f_10000[0], f_15000[0], f_25000[0], f_50000[0]])
f_1 = np.array([f_10000[1], f_15000[1], f_25000[1], f_50000[1]])
f_2 = np.array([f_10000[2], f_15000[2], f_25000[2], f_50000[2]])


N = 4
ind = np.arange(N)  # the x locations for the groups
width = 0.225      # the width of the bars


fig = plt.figure()
ax = fig.add_subplot(111)

rects1 = ax.bar(ind, f_0, width, color='b')
rects2 = ax.bar(ind+width, f_1, width, color='orange')
rects3 = ax.bar(ind+width*2, f_2, width, color='g')

ax.set_title("Time vs Training Samples")
ax.set_ylabel('Time (s)')
ax.set_ylim(0,2000)
ax.set_xlabel('Training Samples (M)')
ax.set_xticks(ind+width)
ax.set_xticklabels(('10,000','15,000','25,000','50,000') )
ax.legend((rects1[0], rects2[0], rects3[0]), ('T 1', 'T 2', 'T 3'))


def autolabel(rects):
    for rect in rects:
        h = rect.get_height()
        ax.text(rect.get_x() + rect.get_width()/2., 1.05*h, '%d'%int(h),
                ha='center', va='bottom')

autolabel(rects1)
autolabel(rects2)
autolabel(rects3)


