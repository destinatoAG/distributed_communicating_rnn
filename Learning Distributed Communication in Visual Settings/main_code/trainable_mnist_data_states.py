# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 04:03:25 2019

@author: priudu
"""

from thop import profile
from thop import clever_format
from pthflops import count_ops
from torchsummary import summary

import torch
import torch.nn as nn
import torch.nn.functional as F
from trainable_mnist_distributed_data_loader import load_mnist_data
from torch.autograd import Variable 
from adjacency_matrix import adj_matrix
from ptflops import get_model_complexity_info

g = { "1" : ["2", "5"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "1"],
        }
       
         
num_nodes = len(g)
feature_space_size = 10

net_config = adj_matrix(g)[0]
net_matrix = adj_matrix(g)[1]

net_matrix_torch = torch.autograd.Variable(torch.from_numpy(net_matrix)).float()

node_1_train_data, node_1_test_data = load_mnist_data()
node_2_train_data, node_2_test_data = load_mnist_data() 
node_3_train_data, node_3_test_data = load_mnist_data() 
node_4_train_data, node_4_test_data = load_mnist_data() 
node_5_train_data, node_5_test_data = load_mnist_data() 

class Net(nn.Module):
    
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, feature_space_size)
        self.fc3 = nn.Linear(feature_space_size,10)
     
    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        exit_node_comm = x
    
        return exit_node_comm 
    

network = Net()

continued_network = Net()  

macs, params = get_model_complexity_info(continued_network, (1, 28, 28), as_strings=True,
                                           print_per_layer_stat=True, verbose=True)
print('{:<30}  {:<8}'.format('Computational complexity: ', macs))
print('{:<30}  {:<8}'.format('Number of parameters: ', params))


data1 = torch.randn(1, 1, 28, 28)
macs, params = profile(continued_network, inputs=(data1, )) 

macs, params = clever_format([macs, params], "%.3f")                    

print('Macs: ', macs)
print('Params: ', params)


inp = torch.rand(1,1,28,28)

# Count the number of FLOPs
count_ops(continued_network, inp)
summary(continued_network, (1, 28, 28)) 

def node_data():
    
    for (batch_idx1, (data1, target1)), (batch_idx2, (data2, target2)), (batch_idx3, (data3, target3)), (batch_idx4, (data4, target4)), (batch_idx5, (data5, target5)) in zip(enumerate(node_1_train_data), enumerate(node_2_train_data), enumerate(node_3_train_data),  enumerate(node_4_train_data),  enumerate(node_5_train_data)): 
        
        node1_features = continued_network(data1)
        node1_targets = target1 
    
        '''
        macs, params = profile(continued_network, inputs=(data1, )) 
        
        macs, params = clever_format([macs, params], "%.3f")                    

        print('Macs: ', macs)
        print('Params: ', params)
        '''
    
        node2_features = continued_network(data2)
        node2_targets = target2 
    
        node3_features = continued_network(data3)
        node3_targets = target3 
    
        node4_features = continued_network(data4)
        node4_targets = target4 
    
        node5_features = continued_network(data5)
        node5_targets = target5 
    
    return node1_features, node1_targets, node2_features, node2_targets, node3_features, node3_targets, node4_features, node4_targets, node5_features, node5_targets
 

def test_data():
    
    for (batch_idx1, (data1, target1)), (batch_idx2, (data2, target2)), (batch_idx3, (data3, target3)), (batch_idx4, (data4, target4)), (batch_idx5, (data5, target5)) in zip(enumerate(node_1_test_data), enumerate(node_2_test_data), enumerate(node_3_test_data),  enumerate(node_4_test_data),  enumerate(node_5_test_data)): 

        node1_features = continued_network(data1)
        node1_targets = target1 
    
        node2_features = continued_network(data2)
        node2_targets = target2 
    
        node3_features = continued_network(data3)
        node3_targets = target3 
    
        node4_features = continued_network(data4)
        node4_targets = target4 
    
        node5_features = continued_network(data5)
        node5_targets = target5 
    
    
    return node1_features, node1_targets, node2_features, node2_targets, node3_features, node3_targets, node4_features, node4_targets, node5_features, node5_targets
 
       
def states_pool(num_nodes, state_size): 
    py_init_states = Variable(torch.zeros(num_nodes, state_size))
    
    return py_init_states
