# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 15:40:51 2019

@author: priudu
"""


import torch
from experiment_1_data_states import num_nodes, net_matrix_torch, feature_space_size
#from mnist_distributed_data_states import num_nodes, net_matrix_torch, feature_space_size

comm_size = 1
node_connections = 2 

input_comms_start_position = feature_space_size
input_comms_end_position = node_connections*comm_size + feature_space_size
output_comms_start_position = feature_space_size + node_connections*comm_size 
output_comms_end_position = feature_space_size + (3*comm_size) 
        
state_size = feature_space_size + 3*comm_size + int(comm_size) 
net_matrix_torch = net_matrix_torch.view(-1, 1).repeat(1, comm_size).view(num_nodes, num_nodes*comm_size)


def comm_states(new_states):
    
    #print('Looking at just arrived py_states from mnist comms handler: ', new_states)
    
    comm_out = torch.tensor([])

    for i in range(num_nodes): 
    
        comm = new_states[i][output_comms_start_position:output_comms_end_position]
        
        #print('Individual Comm States, Node ', i+1, ': ', comm)
        
        comm_out = torch.cat((comm_out,comm))
        
    #print('Looking at comm out before Net Matrix torch, before lossy: ', comm_out)
    
     #making network lossy at 40% loss (2/5) nodes lossy 
    #comm_out[0] = 0.0 
    #comm_out[4] = 0.0
        
    #print('Looking at comm out before Net Matrix torch, after lossy: ', comm_out)
     
    #print('Looking at net matrix torch: ', net_matrix_torch)
    
    comms_out = net_matrix_torch * comm_out
    
    #print('Looking at Comm Out after net matrix torch', comms_out)
    
    for k in range(num_nodes):
        
        node_comm = comms_out[k][comms_out[k].nonzero()].view(-1) 
        
        #print('Looking at Individual Node Input Communication, Node ', k+1, ': ', node_comm)
        
        zeros = torch.zeros(1)
        
        for a in range(comm_size*2):
            if node_comm.shape[0] != comm_size*2: 
                node_comm = torch.cat((node_comm, zeros))  
        
        new_states[k][input_comms_start_position:input_comms_end_position] = node_comm
        
    #print('Looking at pystates with successful input comms: ', new_states)
      
    return new_states 

