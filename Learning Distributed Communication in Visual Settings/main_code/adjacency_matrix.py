
import pandas as pd 


def adj_matrix(g):
    # clean up the example
    g = {k: [v.strip() for v in vs] for k, vs in g.items()}
    
    edges = [(a, b) for a, bs in g.items() for b in bs]
    
    df = pd.DataFrame(edges)
    
    adj_matrix = pd.crosstab(df[0], df[1])
    
    matrix =  adj_matrix.values
    
    return g, matrix







