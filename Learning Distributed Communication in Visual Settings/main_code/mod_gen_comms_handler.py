# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 15:40:51 2019

@author: priudu
"""

import torch
from mod_gen_data_states import feature_space_size

comm_size = 1 
node_connections = 2 

input_comms_start_position = feature_space_size
input_comms_end_position = node_connections*comm_size + feature_space_size
output_comms_start_position = feature_space_size + node_connections*comm_size 
output_comms_end_position = feature_space_size + (3*comm_size) 
        
state_size = feature_space_size + 3*comm_size + int(comm_size) 

def comm_states(new_states, num_nodes, net_matrix_torch):
    
    net_matrix_torch = net_matrix_torch.reshape(-1, 1).repeat(1, comm_size).reshape(num_nodes, num_nodes*comm_size)

    comm_out = torch.tensor([])

    for i in range(num_nodes): 
    
        comm = new_states[i][output_comms_start_position:output_comms_end_position]

        comm_out = torch.cat((comm_out,comm))
        
    comms_out = net_matrix_torch * comm_out
    
    for k in range(num_nodes):
        
        node_comm = comms_out[k][comms_out[k].nonzero()].view(-1) 

        zeros = torch.zeros(1)
        
        for a in range(comm_size*2):
            if node_comm.shape[0] != comm_size*2: 
                node_comm = torch.cat((node_comm, zeros))
        
        new_states[k][input_comms_start_position:input_comms_end_position] = node_comm

    return new_states 




