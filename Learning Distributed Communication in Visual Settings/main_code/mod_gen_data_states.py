# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 04:03:25 2019

@author: priudu
"""

import torch
import torch.nn as nn
import torch.nn.functional as F
from mod_gen_distributed_data_loader import load_mnist_data
from torch.autograd import Variable 


g_2 = { "1" : ["2"],
          "2" : ["1"],
        
        }

g_3 = { "1" : ["2", "3"],
          "2" : ["1", "3"],
          "3" : ["2","1"],
      
        }


g_5 = { "1" : ["2", "5"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "1"],
        }

g_6 = { "1" : ["2", "6"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "6"],
          "6" : ["5", "1"],
        }

g_7 = { "1" : ["2", "7"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "6"],
          "6" : ["5","7"],
          "7" : ["6", "1"], 
        }




g_8 = { "1" : ["2", "8"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "6"],
          "6" : ["5","7"],
          "7" : ["6", "8"], 
          "8" : ["7", "1"],
        }

nodes_list = [g_2, g_3, g_5, g_6, g_7, g_8]

feature_space_size = 25

node_1_train_data, node_1_test_data = load_mnist_data()
node_2_train_data, node_2_test_data = load_mnist_data() 
node_3_train_data, node_3_test_data = load_mnist_data() 
node_4_train_data, node_4_test_data = load_mnist_data() 
node_5_train_data, node_5_test_data = load_mnist_data() 
node_6_train_data, node_6_test_data = load_mnist_data() 
node_7_train_data, node_7_test_data = load_mnist_data() 
node_8_train_data, node_8_test_data = load_mnist_data() 

class Net(nn.Module):
    
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, feature_space_size)
        self.fc3 = nn.Linear(feature_space_size,10)
     
    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        exit_node_comm = x
    
        return exit_node_comm 
    

network = Net()

network = Net()

continued_network = Net()  

network_state_dict = torch.load('../models/25f_1c_8n_model.pth')
continued_network.load_state_dict(network_state_dict)

def node_data():
    
    for (batch_idx1, (data1, target1)), (batch_idx2, (data2, target2)), (batch_idx3, (data3, target3)), (batch_idx4, (data4, target4)), (batch_idx5, (data5, target5)), (batch_idx6, (data6, target6)), (batch_idx7, (data7, target7)), (batch_idx8, (data8, target8)) in zip(enumerate(node_1_train_data), enumerate(node_2_train_data), enumerate(node_3_train_data),  enumerate(node_4_train_data),  enumerate(node_5_train_data),  enumerate(node_6_train_data),  enumerate(node_7_train_data),  enumerate(node_8_train_data)): 

        node1_features = continued_network(data1)
        node1_targets = target1 
    
        node2_features = continued_network(data2)
        node2_targets = target2 
    
        node3_features = continued_network(data3)
        node3_targets = target3 
    
        node4_features = continued_network(data4)
        node4_targets = target4 
    
        node5_features = continued_network(data5)
        node5_targets = target5 
    
        node6_features = continued_network(data6)
        node6_targets = target6 
    
        node7_features = continued_network(data7)
        node7_targets = target7 
    
        node8_features = continued_network(data8)
        node8_targets = target8
    
    return node1_features, node1_targets, node2_features, node2_targets, node3_features, node3_targets, node4_features, node4_targets, node5_features, node5_targets, node6_features, node6_targets, node7_features, node7_targets, node8_features, node8_targets
 

def test_data():
    
    for (batch_idx1, (data1, target1)), (batch_idx2, (data2, target2)), (batch_idx3, (data3, target3)), (batch_idx4, (data4, target4)), (batch_idx5, (data5, target5)), (batch_idx6, (data6, target6)), (batch_idx7, (data7, target7)), (batch_idx8, (data8, target8)) in zip(enumerate(node_1_test_data), enumerate(node_2_test_data), enumerate(node_3_test_data),  enumerate(node_4_test_data),  enumerate(node_5_test_data),  enumerate(node_6_test_data),  enumerate(node_7_test_data),  enumerate(node_8_test_data)): 

        node1_features = continued_network(data1)
        node1_targets = target1 
    
        node2_features = continued_network(data2)
        node2_targets = target2 
    
        node3_features = continued_network(data3)
        node3_targets = target3 
    
        node4_features = continued_network(data4)
        node4_targets = target4 
    
        node5_features = continued_network(data5)
        node5_targets = target5 
    
        node6_features = continued_network(data6)
        node6_targets = target6 
    
        node7_features = continued_network(data7)
        node7_targets = target7 
    
        node8_features = continued_network(data8)
        node8_targets = target8
    
    return node1_features, node1_targets, node2_features, node2_targets, node3_features, node3_targets, node4_features, node4_targets, node5_features, node5_targets, node6_features, node6_targets, node7_features, node7_targets, node8_features, node8_targets
 
       
def states_pool(num_nodes, state_size): 
    py_init_states = Variable(torch.zeros(num_nodes, state_size))
    return py_init_states
