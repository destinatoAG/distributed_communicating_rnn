#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 18 14:46:52 2021

@author: pabudu
"""

import torch
from math import sqrt
import numpy as np
from error_rate import error_rate 
from sklearn.metrics import mean_squared_error, r2_score


def test(net, test_runs, test_node_inputs, test_node_targets, feature_space_size, accuracy_criterion):    
    
    running_loss = 0.0
    
    test_node1_inputs = test_node_inputs[0]
    test_node2_inputs = test_node_inputs[1]
    test_node3_inputs = test_node_inputs[2]
    test_node4_inputs = test_node_inputs[3]
    test_node5_inputs = test_node_inputs[4]
    
    test_node1_targets = test_node_targets[0]
    test_node2_targets = test_node_targets[1]
    test_node3_targets = test_node_targets[2]
    test_node4_targets = test_node_targets[3]
    test_node5_targets = test_node_targets[4]
    
    test_predictions = np.asarray([])
    test_target = np.asarray([])
    pred_write = np.asarray([])
    step_mse = np.asarray([])
    step_error = np.asarray([])
    step_losses = np.asarray([])
    all_losses = np.asarray([])
        
    net.eval()
    
    with torch.no_grad():  
    
        for k in range(test_runs):
            
            input_data = torch.tensor([]) 
            input_data = torch.cat((input_data, test_node1_inputs[k].view(1,feature_space_size)), 0) 
            input_data = torch.cat((input_data, test_node2_inputs[k].view(1,feature_space_size)), 0)
            input_data = torch.cat((input_data, test_node3_inputs[k].view(1,feature_space_size)), 0)
            input_data = torch.cat((input_data, test_node4_inputs[k].view(1,feature_space_size)), 0) 
            input_data = torch.cat((input_data, test_node5_inputs[k].view(1,feature_space_size)), 0)  
            
            node1_target = torch.tensor([test_node1_targets[k]]) 
            node2_target = torch.tensor([test_node2_targets[k]]) 
            node3_target = torch.tensor([test_node3_targets[k]])
            node4_target = torch.tensor([test_node4_targets[k]]) 
            node5_target = torch.tensor([test_node5_targets[k]])  
        
            all_targets = torch.cat((node1_target, node2_target, node3_target, node4_target, node5_target))
        
            target = torch.max(all_targets)
            
            target = target.float()
            
            target = target / 10
            
            output, comms_count, all_node_count = net(input_data)
       
            pred_write = np.append(pred_write, output.data)
            
            #target = parity(target)
            
            test_predictions = np.append(test_predictions, output.data)
            
            test_target = np.append(test_target, target.data)
            
            test_predictions = test_predictions.round(1)
            test_target = test_target.round(1)
            
            loss = accuracy_criterion(output,target)
            
            running_loss += loss.data
        
            if k % 100 == 99:    # print every 1000 mini-batches
                print('[%5d] loss: %.3f' %
                  (k + 1, running_loss / 100))
                losses = running_loss/100
                step_losses = np.append(step_losses, losses)
                running_loss = 0.0
                mses = mean_squared_error(test_target, test_predictions)
                error1 = error_rate(test_predictions, test_target)
                step_mse = np.append(step_mse,mses)
                step_error = np.append(step_error,error1)
            all_losses = np.append(all_losses, loss.data)
            
    mse = mean_squared_error(test_target, test_predictions)
    rmse = sqrt(mse)
    variance = r2_score(test_target, test_predictions)
    error = error_rate(test_predictions, test_target)
      
    return step_losses, mse, rmse, variance, all_losses, test_predictions, test_target, error, pred_write, step_mse, step_error

def test_mod_gen(num_nodes, net_matrix_torch, test_runs, net, test_node_inputs, test_node_targets, feature_space_size, accuracy_criterion):
    
    #test_loss = 0.0
    test_predictions = np.asarray([])
    test_target = np.asarray([])
    pred_write = np.asarray([])
    step_mse = np.asarray([])
    running_loss = 0.0
    step_losses = np.asarray([])
    all_losses = np.asarray([])
    
    test_node1_inputs = test_node_inputs[0]
    test_node2_inputs = test_node_inputs[1]
    test_node3_inputs = test_node_inputs[2]
    test_node4_inputs = test_node_inputs[3]
    test_node5_inputs = test_node_inputs[4]
    test_node6_inputs = test_node_inputs[5]
 
    test_node1_targets = test_node_targets[0]
    test_node2_targets = test_node_targets[1]
    test_node3_targets = test_node_targets[2]
    test_node4_targets = test_node_targets[3]
    test_node5_targets = test_node_targets[4]
    test_node6_targets = test_node_targets[5]
        
    net.eval()
    
    with torch.no_grad():  
    
        for k in range(test_runs):
            
            input_data = torch.tensor([]) 
            input_data = torch.cat((input_data, test_node1_inputs[k].view(1,feature_space_size)), 0) 
            input_data = torch.cat((input_data, test_node2_inputs[k].view(1,feature_space_size)), 0)
            input_data = torch.cat((input_data, test_node3_inputs[k].view(1,feature_space_size)), 0)
            input_data = torch.cat((input_data, test_node4_inputs[k].view(1,feature_space_size)), 0) 
            input_data = torch.cat((input_data, test_node5_inputs[k].view(1,feature_space_size)), 0)
            input_data = torch.cat((input_data, test_node6_inputs[k].view(1,feature_space_size)), 0) 
                 
            node1_target = torch.tensor([test_node1_targets[k]]) 
            node2_target = torch.tensor([test_node2_targets[k]]) 
            node3_target = torch.tensor([test_node3_targets[k]])
            node4_target = torch.tensor([test_node4_targets[k]]) 
            node5_target = torch.tensor([test_node5_targets[k]]) 
            node6_target = torch.tensor([test_node6_targets[k]]) 
      
            all_targets = torch.cat((node1_target, node2_target, node3_target, node4_target, node5_target, node6_target))
        
            target = torch.max(all_targets)
            
            target = target.float()
            
            target = target / 10
            
            output = net(input_data, num_nodes, net_matrix_torch)
            
            pred_write = np.append(pred_write, output.data)
            
            #target = parity(target)
            
            test_predictions = np.append(test_predictions, output.data)
            
            test_target = np.append(test_target, target.data)
            
            test_predictions = test_predictions.round(1)
            test_target = test_target.round(1)
            
            loss = accuracy_criterion(output,target)
            
            running_loss += loss.data
        
            if k % 100 == 99:    # print every 1000 mini-batches
                print('[%5d] loss: %.3f' %
                  (k + 1, running_loss / 100))
                losses = running_loss/100
                step_losses = np.append(step_losses, losses)
                running_loss = 0.0
                mses = mean_squared_error(test_target, test_predictions)
                step_mse = np.append(step_mse,mses)
                
            all_losses = np.append(all_losses, loss.data)
            
    mse = mean_squared_error(test_target, test_predictions)
    rmse = sqrt(mse)
    variance = r2_score(test_target, test_predictions)
    error = error_rate(test_predictions, test_target)
      
    return step_losses, mse, rmse, variance, all_losses, test_predictions, test_target, error, pred_write, step_mse
