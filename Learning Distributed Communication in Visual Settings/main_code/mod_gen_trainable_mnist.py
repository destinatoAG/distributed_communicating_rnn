# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 16:21:33 2019

@author: priudu
"""

#16% faster that previous model 

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F 
import matplotlib.pyplot as plt 
from mod_gen_data_states import states_pool, feature_space_size, nodes_list
from mod_gen_comms_handler import comm_states, comm_size
from datetime import datetime
import numpy as np
from sklearn.metrics import mean_squared_error
from math import sqrt
from adjacency_matrix import adj_matrix
from evaluator import test_mod_gen

state_size = feature_space_size + 3*comm_size + int(comm_size)  

print('Staging Data for...') 
print('Number of MNIST Input Features: ', feature_space_size)
print('Overall Node State size: ', state_size)
print('Communication channel size: ', comm_size)

data_start=datetime.now() 
from mod_gen_data_states import node_data

all_data = node_data() 

node1_inputs = all_data[0]
node1_targets = all_data[1]
node2_inputs = all_data[2]
node2_targets = all_data[3]
node3_inputs = all_data[4]
node3_targets = all_data[5]
node4_inputs = all_data[6]
node4_targets = all_data[7]
node5_inputs = all_data[8]
node5_targets = all_data[9]


node_time = datetime.now()-data_start

print()
print('Finished Staging Node Train Data!')
print ("Time taken to stage node train data: %s" %(node_time))

timesteps = 5

class Node(nn.Module):

    def __init__(self):
        super(Node, self).__init__()
        # These are the weights (common) in the whole network       
        self.fc1 = nn.Linear(state_size, 50)
        self.fc2 = nn.Linear(50, 8)
        self.fc3 = nn.Linear(8, state_size)
        
    def step(self,state):
        """ This is an internal function which runs a single step/iteration of a node"""
        "tied weights here"
        
        state = F.relu(F.linear(state,self.fc1.weight,self.fc1.bias))
        state =  F.relu(F.linear(state,self.fc2.weight,self.fc2.bias))
        state =  F.linear(state,self.fc3.weight,self.fc3.bias)
        return state
    
    def forward(self, input_data, num_nodes, net_matrix_torch):
        """ This takes in an input sequence and uses it to run through the network"""

        py_states = states_pool(num_nodes, state_size)

        for k in range(timesteps): 

            for k in range(num_nodes):
                #data input        
                py_states[k][0:feature_space_size] = input_data[k]
            
            #communication 
            py_states = comm_states(py_states, num_nodes, net_matrix_torch)

            #step update 
            for k in range(num_nodes):
                
                py_states[k] = self.step(py_states[k].clone())
            
        return py_states[0][-1]

net = Node()
net.zero_grad() 

criterion = nn.MSELoss()

runs = 300
lr = 0.005

optimizer = optim.Adam(net.parameters(), lr)

params = list(net.parameters())

#Data attributes 
def count_parameters(net):
    return sum(p.numel() for p in net.parameters() if p.requires_grad)

params_trainable = count_parameters(net)
num_neurons = 8
params_all = sum(p.numel() for p in net.parameters())
params = list(net.parameters())
layers_total = len(params)
hidden_total = layers_total - 2

print()
print('Number of timesteps: ', timesteps)
print('Total Number of Layers: ', layers_total)
print('Hidden Layers: ', hidden_total)
print('Parameters Number - Trainable: ', params_trainable)
print('Parameters Number - ALL: ', params_all)
print('Learning rate: ', lr)
print('Number of training samples: ', runs)


def parity(t):
    if (t % 2) == 0:
        t = torch.tensor(0.0)
    else:
        t = torch.tensor(1.0)
    return t 

def net_matrix(graph_pos, num_nodes):
    
    net_configs = adj_matrix(nodes_list[graph_pos])[0]
    net_matrix = adj_matrix(nodes_list[graph_pos])[1]
    net_matrix_torch = torch.autograd.Variable(torch.from_numpy(net_matrix)).float()
    
    return net_matrix_torch, net_configs, num_nodes
    
net_matrix_torch, net_configs, num_nodes = net_matrix(0,2)

# Now for the training
train_start=datetime.now() 

predictions = np.asarray([])
step_losses = np.asarray([])
targets = np.asarray([])

running_loss = 0.0
losses = 0

t_new = np.asarray([])
all_losses = np.asarray([])

print('Now training on 2 nodes')

runs = 400

for k in range(runs): 
    
    input_data = torch.tensor([]) 
    input_data = torch.cat((input_data, node1_inputs[k].view(1,feature_space_size)), 0) 
    input_data = torch.cat((input_data, node2_inputs[k].view(1,feature_space_size)), 0)
 
    node1_target = torch.tensor([node1_targets[k]]) 
    node2_target = torch.tensor([node2_targets[k]]) 
 
    all_targets = torch.cat((node1_target, node2_target))

    target = torch.max(all_targets)
    
    target = target.float()
    
    target = target / 10
    #target = parity(target)
    
    output = net(input_data, num_nodes, net_matrix_torch)
    
    optimizer.zero_grad()   
    
    predictions = np.append(predictions, output.data)
    targets = np.append(targets, target.data)
    
    loss = criterion(output, target)

    loss.backward(retain_graph=True)
    
    optimizer.step()    
    
    running_loss += loss.data
    
    if k % 100 == 99:    # print every 1000 mini-batches
        print('[%5d] loss: %.3f' %
              (k + 1, running_loss / 100))
        losses = running_loss/100
        step_losses = np.append(step_losses, losses)
        running_loss = 0.0

    all_losses = np.append(all_losses, loss.data)

train_mse = mean_squared_error(targets, predictions)
train_rmse = sqrt(train_mse)

train_time = datetime.now()-train_start
print ("Time to Train Network %s" %(train_time))


print('Now training on 3 nodes')

net_matrix_torch, net_configs, num_nodes = net_matrix(1,3)


for k in range(runs): 
    
    input_data = torch.tensor([]) 
    input_data = torch.cat((input_data, node1_inputs[k].view(1,feature_space_size)), 0) 
    input_data = torch.cat((input_data, node2_inputs[k].view(1,feature_space_size)), 0)
    input_data = torch.cat((input_data, node3_inputs[k].view(1,feature_space_size)), 0)
 
    node1_target = torch.tensor([node1_targets[k]]) 
    node2_target = torch.tensor([node2_targets[k]]) 
    node3_target = torch.tensor([node3_targets[k]]) 
 
    all_targets = torch.cat((node1_target, node2_target, node3_target))

    target = torch.max(all_targets)
    
    target = target.float()
    
    target = target / 10
    #target = parity(target)
    
    output = net(input_data, num_nodes, net_matrix_torch)
    
    optimizer.zero_grad()   
    
    predictions = np.append(predictions, output.data)
    targets = np.append(targets, target.data)
    
    loss = criterion(output, target)

    loss.backward(retain_graph=True)
    
    optimizer.step()    
    
    running_loss += loss.data
    
    if k % 100 == 99:    # print every 1000 mini-batches
        print('[%5d] loss: %.3f' %
              (k + 1, running_loss / 100))
        losses = running_loss/100
        step_losses = np.append(step_losses, losses)
        running_loss = 0.0

    all_losses = np.append(all_losses, loss.data)
    

train_mse = mean_squared_error(targets, predictions)
train_rmse = sqrt(train_mse)

train_time = datetime.now()-train_start
print ("Time to Train Network %s" %(train_time))

print('Now training on 5 nodes')

net_matrix_torch, net_configs, num_nodes = net_matrix(2,5)


for k in range(runs): 
    
    input_data = torch.tensor([]) 
    input_data = torch.cat((input_data, node1_inputs[k].view(1,feature_space_size)), 0) 
    input_data = torch.cat((input_data, node2_inputs[k].view(1,feature_space_size)), 0)
    input_data = torch.cat((input_data, node3_inputs[k].view(1,feature_space_size)), 0)
    input_data = torch.cat((input_data, node4_inputs[k].view(1,feature_space_size)), 0) 
    input_data = torch.cat((input_data, node5_inputs[k].view(1,feature_space_size)), 0)
    
    node1_target = torch.tensor([node1_targets[k]]) 
    node2_target = torch.tensor([node2_targets[k]]) 
    node3_target = torch.tensor([node3_targets[k]])
    node4_target = torch.tensor([node4_targets[k]]) 
    node5_target = torch.tensor([node5_targets[k]]) 
 
    all_targets = torch.cat((node1_target, node2_target, node3_target, node4_target, node5_target))

    target = torch.max(all_targets)
    
    target = target.float()
    
    target = target / 10
    #target = parity(target)
    
    output = net(input_data, num_nodes, net_matrix_torch)
    
    optimizer.zero_grad()   
    
    predictions = np.append(predictions, output.data)
    targets = np.append(targets, target.data)
    
    loss = criterion(output, target)

    loss.backward(retain_graph=True)
    
    optimizer.step()    
    
    running_loss += loss.data
    
    if k % 100 == 99:    # print every 1000 mini-batches
        print('[%5d] loss: %.3f' %
              (k + 1, running_loss / 100))
        losses = running_loss/100
        step_losses = np.append(step_losses, losses)
        running_loss = 0.0

    all_losses = np.append(all_losses, loss.data)

train_mse = mean_squared_error(targets, predictions)
train_rmse = sqrt(train_mse)

train_time = datetime.now()-train_start
print ("Time to Train Network %s" %(train_time))

print('Staging Test Data...') 
test_start=datetime.now() 
from mod_gen_data_states import test_data

test_data = test_data() 

test_node1_inputs = test_data[0]
test_node1_targets = test_data[1]
test_node2_inputs = test_data[2]
test_node2_targets = test_data[3]
test_node3_inputs = test_data[4]
test_node3_targets = test_data[5]
test_node4_inputs = test_data[6]
test_node4_targets = test_data[7]
test_node5_inputs = test_data[8]
test_node5_targets = test_data[9]
test_node6_inputs = test_data[10]
test_node6_targets = test_data[11]

test_node_inputs = [test_node1_inputs, test_node2_inputs, test_node3_inputs, test_node4_inputs, test_node5_inputs, test_node6_inputs]
test_node_targets = [test_node1_targets, test_node2_targets, test_node3_targets, test_node4_targets, test_node5_targets, test_node6_targets]

test_time = datetime.now()-test_start

print()
print('Finished Staging Node Data!')
print ("Time taken to stage node data: %s" %(test_time))

test_runs = 200

net_matrix_torch, net_configs, num_nodes = net_matrix(3,6)

print('Now testing Network on ', num_nodes, ' nodes...')
# Now for the training
test2_start=datetime.now() 

x = test_mod_gen(num_nodes, net_matrix_torch, test_runs, net, test_node_inputs, test_node_targets, feature_space_size, criterion)
test2_time = datetime.now()-test2_start

print()
print('Finished Testing')
print ("Time taken to Test Network: %s" %(test2_time))

test_losses = x[0].tolist()
mse = x[1]
rmse = x[2]
variance = x[3]
all_test_losses = x[4].tolist() 
test_predictions = x[5].tolist()
test_target = x[6].tolist()
error = x[7]
pred_write = x[8].tolist()
step_mse = x[9].tolist() 

print('Task: Max')
print('Results on test set!')
print('MSE achieved: ', mse)  
print('RMSE achieved: ', rmse)
print('Variance achieved: ', variance)
print('Error Rate: ', error)

file = open('results_data.txt', 'w')

file.writelines('Task: Max \n')
file.writelines('Number of Network Nodes: ' + repr(num_nodes) + '\n')
file.writelines('Number of MNIST Input Features: ' + repr(feature_space_size) + '\n') 
file.writelines('Overall Node State size: ' + repr(state_size) + '\n')
file.writelines('Communication channel size: ' + repr(comm_size) + '\n')
file.writelines('Number of Timesteps: ' + repr(timesteps) + '\n')
file.writelines('Total Number of Layers: ' + repr(layers_total) + '\n')
file.writelines('Hidden Layers: ' + repr(hidden_total) + '\n') 
file.writelines('Parameters Number - Trainable: ' + repr(params_trainable) + '\n')
file.writelines('Parameters Number - ALL: ' + repr(params_all) + '\n')
file.writelines('Number of training samples: ' + repr(runs) + '\n') 
file.writelines('Number of testing samples: ' + repr(test_runs) + '\n') 
file.writelines('Training Losses: ' + repr(step_losses) + '\n') 
file.writelines('Test Losses: ' + repr(test_losses) + '\n') 
file.writelines('Time taken to stage node train data: ' + repr(node_time) + '\n') 
file.writelines('Time taken to train network: ' + repr(train_time) + '\n') 
file.writelines('Time taken to stage node test data: ' + repr(test_time) + '\n') 
file.writelines('Time taken to test network: ' + repr(test2_time) + '\n') 
file.writelines('Lowest Train Loss, at ' + repr(k+1) + ' steps/epochs: ' + repr(step_losses[-1]) + '\n') 
file.writelines('MSE achieved: ' + repr(mse) + '\n') 
file.writelines('RMSE achieved: ' + repr(rmse) + '\n') 
file.writelines('Variance: ' + repr(variance) + '\n') 
file.writelines('Error Rate: ' + repr(error) + '\n') 
file.writelines('Train MSE achieved: ' + repr(train_mse) + '\n') 
file.writelines('Train RMSE achieved: ' + repr(train_rmse) + '\n') 
file.writelines('Original Predictions: ' + repr(pred_write) + '\n') 
file.writelines('Original/Rounded Targets: ' + repr(test_target) + '\n') 
file.writelines('Rounded Predictions: ' + repr(test_predictions) + '\n') 
file.writelines('Step MSE: ' + repr(step_mse) + '\n') 
file.close()

fig_13, ax6 = plt.subplots(figsize=(5,3))
plt.title('Training Losses')
plt.legend(['Train Loss'], loc='upper right')
plt.plot(step_losses, color='blue')
plt.show(ax6)

fig_14, ax7 = plt.subplots(figsize=(5,3))
plt.title('All Losses')
plt.plot(all_losses)
plt.show(ax7)
