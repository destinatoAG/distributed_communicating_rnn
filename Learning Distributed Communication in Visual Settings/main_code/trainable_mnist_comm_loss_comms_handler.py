import torch
import numpy as np 
from torch.autograd import Variable 
from commloss_repo import comm_decider 
from trainable_mnist_data_states import num_nodes, net_matrix_torch, feature_space_size
#from mnist_distributed_data_states import num_nodes, net_matrix_torch, feature_space_size

comm_size = 1 
node_connections = 2 

input_comms_start_position = feature_space_size
input_comms_end_position = (node_connections*comm_size) - 1 + feature_space_size 
output_comms_start_position = feature_space_size + (node_connections*comm_size) - 1 
output_comms_end_position = feature_space_size + (3*comm_size) - 1 
        
state_size = feature_space_size + 3*comm_size + int(comm_size) 

net_matrix_torch = net_matrix_torch.reshape(-1, 1).repeat(1, comm_size).reshape(num_nodes, num_nodes*comm_size)

def comm_states(new_states):
    
    #print('Looking at just arrived py_states from mnist comms handler: ', new_states)
    comms_count = 0.0
    count = 0
    node_count = np.asarray([])
    
    comm_out = torch.tensor([])

    for i in range(num_nodes): 
    
        comm = new_states[i][output_comms_start_position:output_comms_end_position]
        comm_out = torch.cat((comm_out,comm))
          
    #Modelling lossy links 
    #comm_out[0] = 0.0
    #comm_out[4] = 0.0 

    comms_out = net_matrix_torch * comm_out

    for k in range(num_nodes):
        
        node_comm = comms_out[k][comms_out[k].nonzero()].view(-1) 
        
        node_comm, indices = torch.max(comms_out[k], 0)
        
        node_comm = comm_decider(node_comm)

        if node_comm != 0.0 or node_comm != -0.0:
             comms_count += 1
               
        zeros = torch.zeros(1)
        
        for a in range(comm_size*1):
            if node_comm.shape[0] != comm_size*1: 
                node_comm = torch.cat((node_comm, zeros))
        
        new_states[k][input_comms_start_position:input_comms_end_position] = node_comm
        
        if node_comm != 0.0:
            count = 1
            node_count = np.append(node_count,count)
        else:
            count = 0 
            node_count = np.append(node_count,count)
                
    return new_states, comms_count, node_count 
















