# -*- coding: utf-8 -*-
"""
Created on Tue Jul 23 11:14:46 2019

@author: priudu
"""

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

#from pthflops import count_ops
#from thop import profile
#from thop import clever_format

import torch
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F 
import matplotlib.pyplot as plt 
from trainable_mnist_data_states import states_pool, num_nodes, feature_space_size
from trainable_mnist_comm_loss_comms_handler import comm_states, comm_size
from commloss_repo import CommLoss
from evaluator import test
from datetime import datetime
import numpy as np
from sklearn.metrics import mean_squared_error
from math import sqrt
from error_rate import error_rate 
state_size = feature_space_size + 3*comm_size + int(comm_size)  

print('Staging Data for...') 
print('Number of Network Nodes: ', num_nodes) 
print('Number of MNIST Input Features: ', feature_space_size)
print('Overall Node State size: ', state_size)
print('Communication channel size: ', comm_size)

data_start=datetime.now() 
from trainable_mnist_data_states import node_data

all_data = node_data() 

node1_inputs = all_data[0]
node1_targets = all_data[1]
node2_inputs = all_data[2]
node2_targets = all_data[3]
node3_inputs = all_data[4]
node3_targets = all_data[5]
node4_inputs = all_data[6]
node4_targets = all_data[7]
node5_inputs = all_data[8]
node5_targets = all_data[9]

node_time = datetime.now()-data_start

print()
print('Finished Staging Node Train Data!')
print ("Time taken to stage node train data: %s" %(node_time))

timesteps = 5

class Node(nn.Module):
     
    def __init__(self):
        super(Node, self).__init__()
        # These are the weights (common) in the whole network       
        self.fc1 = nn.Linear(state_size, 50)
        self.fc2 = nn.Linear(50, 8)
        self.fc3 = nn.Linear(8, state_size)
        
    def step(self,state):
        """ This is an internal function which runs a single step/iteration of a node"""
        "tied weights here"
        
        state = F.relu(F.linear(state,self.fc1.weight,self.fc1.bias))
        state =  F.relu(F.linear(state,self.fc2.weight,self.fc2.bias))
        state =  F.linear(state,self.fc3.weight,self.fc3.bias)
        
        return state
    
  
    
    def forward(self, input_data):
        
        comms_total = 0
          
        """ This takes in an input sequence and uses it to run through the network"""
        
        py_states = states_pool(num_nodes, state_size)
        
        node_count_total = np.asarray([0.0,0.0,0.0,0.0,0.0])
        
        for k in range(timesteps): 
            for k in range(num_nodes):
                #data input        
                py_states[k][0:feature_space_size] = input_data[k]
            
            py_states, comms_count, node_count = comm_states(py_states)
    
            #step update 
            for k in range(num_nodes):
             
                py_states[k] = self.step(py_states[k].clone())
               
            comms_total += comms_count
            node_count_total += node_count
      
        return py_states[0][-1], comms_total, node_count_total 

net = Node()
net.zero_grad() 

accuracy_criterion = nn.MSELoss()

runs = 500
lr = 0.005

optimizer = optim.Adam(net.parameters(), lr)

params = list(net.parameters())

#Data attributes 
def count_parameters(net):
    return sum(p.numel() for p in net.parameters() if p.requires_grad)

params_trainable = count_parameters(net)
num_neurons = 8
params_all = sum(p.numel() for p in net.parameters())
params = list(net.parameters())
layers_total = len(params)
hidden_total = layers_total - 2

print()
print('Number of timesteps: ', timesteps)
print('Total Number of Layers: ', layers_total)
print('Hidden Layers: ', hidden_total)
print('Parameters Number - Trainable: ', params_trainable)
print('Parameters Number - ALL: ', params_all)
print('Learning rate: ', lr)
print('Number of training samples: ', runs)

def comms_target(timesteps):
    target = (1/2 * (num_nodes*timesteps))
    target = round(target,0)
    return target

def parity(t):
    if (t % 2) == 0:
        t = torch.tensor(0.0)
    else:
        t = torch.tensor(1.0)
    return t 


# Now for the training
train_start=datetime.now() 

predictions = np.asarray([])
step_losses = np.asarray([])
targets = np.asarray([])

running_loss = 0.0
running_comm_loss = 0.0
losses = 0
total_comms = 0.0

t_new = np.asarray([])
all_losses = np.asarray([])

all_comm_losses = np.asarray([])
total_comms_list = np.asarray([])
comm_targets = np.asarray([])
total_node_count = np.asarray([0.0,0.0,0.0,0.0,0.0])
step_losses = np.asarray([])
step_comm_losses = np.asarray([])


step_mse = np.asarray([])
step_error = np.asarray([])

for k in range(runs): 
    
    input_data = torch.tensor([]) 
    input_data = torch.cat((input_data, node1_inputs[k].view(1,feature_space_size)), 0) 
    input_data = torch.cat((input_data, node2_inputs[k].view(1,feature_space_size)), 0)
    input_data = torch.cat((input_data, node3_inputs[k].view(1,feature_space_size)), 0)
    input_data = torch.cat((input_data, node4_inputs[k].view(1,feature_space_size)), 0) 
    input_data = torch.cat((input_data, node5_inputs[k].view(1,feature_space_size)), 0)
    
    node1_target = torch.tensor([node1_targets[k]])  
    node2_target = torch.tensor([node2_targets[k]]) 
    node3_target = torch.tensor([node3_targets[k]])
    node4_target = torch.tensor([node4_targets[k]]) 
    node5_target = torch.tensor([node5_targets[k]])  
 
    all_targets = torch.cat((node1_target, node2_target, node3_target, node4_target, node5_target))

    target = torch.max(all_targets)
    
    target_count = 5.0
    
    target = target.float()
    
    target = target / 10
   
    inp = input_data 
    
    '''
    #target = parity(target)
    # Count the number of FLOPs
    count_ops(net, inp)

    macs, params = profile(net, inputs=(inp, )) 
    macs, params = clever_format([macs, params], "%.3f")                    
    print('Macs: ', macs)
    print('Params: ', params)
    '''
    output, comms_count, all_node_count = net(input_data)

    optimizer.zero_grad()   
    
    predictions = np.append(predictions, output.data)
    targets = np.append(targets, target.data)

    loss = accuracy_criterion(output, target)
 
    loss_2 = CommLoss(comms_count, target_count)

    loss = loss + loss_2
    
    loss.backward(retain_graph=True)
    
    optimizer.step()    
    
    running_loss += loss.data
    running_comm_loss += loss_2
    total_comms += comms_count
    total_node_count += all_node_count
    
    if k % 100 == 99:    # print every 100 mini-batches
        print('[%5d] Training loss: %.3f Communication Cost: %.3f'  %
              (k + 1, running_loss / 100, running_comm_loss / 100))
        losses = running_loss/100
        comm_losses = running_comm_loss /100
        step_losses = np.append(step_losses, losses)
        step_comm_losses = np.append(step_comm_losses, comm_losses) 
        total_comms_list = np.append(total_comms_list, total_comms)
        total_node_count_list_1000 = total_node_count
        comm_targets = np.append(comm_targets,  target_count)
        running_loss = 0.0
        running_comm_loss = 0.0
        total_comms = 0.0
        
        train_mses = sqrt(mean_squared_error(targets, predictions))
        train_errors = error_rate(targets, predictions)
        step_mse = np.append(step_mse,train_mses)
        step_error = np.append(step_error,train_errors)
        
        print('Total node comms count after 100 iterations: ', total_node_count/100)
        total_node_count = np.asarray([0.0,0.0,0.0,0.0,0.0])
    all_losses = np.append(all_losses, loss.data)
    all_comm_losses = np.append(all_comm_losses, loss_2)
    

train_mse = mean_squared_error(targets, predictions)
train_rmse = sqrt(train_mse)

train_time = datetime.now()-train_start
print ("Time to Train Network %s" %(train_time))

total_comms_list = total_comms_list/100

print('Total comms list: ', total_comms_list)
print('Step MSE: ', step_mse)
print('Step Error: ', step_error) 


print('\nStaging Test Data...') 
test_start=datetime.now() 
from trainable_mnist_data_states import test_data

test_data = test_data() 

test_node1_inputs = test_data[0]
test_node1_targets = test_data[1]
test_node2_inputs = test_data[2]
test_node2_targets = test_data[3]
test_node3_inputs = test_data[4]
test_node3_targets = test_data[5]
test_node4_inputs = test_data[6]
test_node4_targets = test_data[7]
test_node5_inputs = test_data[8]
test_node5_targets = test_data[9]

test_time = datetime.now()-test_start

print()
print('Finished Staging Node Data!')
print ("Time taken to stage node data: %s" %(test_time))
 
test_runs = 500
test_node_inputs = [test_node1_inputs, test_node2_inputs, test_node3_inputs, test_node4_inputs, test_node5_inputs]
test_node_targets = [test_node1_targets, test_node2_targets, test_node3_targets, test_node4_targets, test_node5_targets]

print('Now testing Network...')
# Now for the training
test2_start=datetime.now() 
x = test(net, test_runs, test_node_inputs, test_node_targets, feature_space_size, accuracy_criterion)
test2_time = datetime.now()-test2_start

print()
print('Finished Testing')
print ("Time taken to Test Network: %s" %(test2_time))


test_losses = x[0].tolist()
mse = x[1]
rmse = x[2]
variance = x[3]
all_test_losses = x[4].tolist() 
test_predictions = x[5].tolist()
test_target = x[6].tolist()
error = x[7]
pred_write = x[8].tolist()
step_mse = x[9].tolist() 
step_error = x[10].tolist()


print('Task: Max')
print('Results on test set!')
print('MSE achieved: ', mse)  
print('RMSE achieved: ', rmse)
print('Error Rate: ', error)


file = open('results_data.txt', 'w')

file.writelines('Task: Max Lossy \n')
file.writelines('Number of Network Nodes: ' + repr(num_nodes) + '\n')
file.writelines('Number of MNIST Input Features: ' + repr(feature_space_size) + '\n') 
file.writelines('Overall Node State size: ' + repr(state_size) + '\n')
file.writelines('Communication channel size: ' + repr(comm_size) + '\n')
file.writelines('Number of Timesteps: ' + repr(timesteps) + '\n')
file.writelines('Total Number of Layers: ' + repr(layers_total) + '\n')
file.writelines('Hidden Layers: ' + repr(hidden_total) + '\n') 
file.writelines('Parameters Number - Trainable: ' + repr(params_trainable) + '\n')
file.writelines('Parameters Number - ALL: ' + repr(params_all) + '\n')
file.writelines('Number of training samples: ' + repr(runs) + '\n') 
file.writelines('Number of testing samples: ' + repr(test_runs) + '\n') 
file.writelines('Training Losses: ' + repr(step_losses) + '\n') 
file.writelines('Test Losses: ' + repr(test_losses) + '\n') 
file.writelines('Time taken to stage node train data: ' + repr(node_time) + '\n') 
file.writelines('Time taken to train network: ' + repr(train_time) + '\n') 
file.writelines('Time taken to stage node test data: ' + repr(test_time) + '\n') 
file.writelines('Time taken to test network: ' + repr(test2_time) + '\n') 
file.writelines('Lowest Train Loss, at ' + repr(k+1) + ' steps/epochs: ' + repr(step_losses[-1]) + '\n') 
file.writelines('MSE achieved: ' + repr(mse) + '\n') 
file.writelines('RMSE achieved: ' + repr(rmse) + '\n') 
file.writelines('Error Rate: ' + repr(error) + '\n') 
file.writelines('Train MSE achieved: ' + repr(train_mse) + '\n') 
file.writelines('Train RMSE achieved: ' + repr(train_rmse) + '\n') 
file.writelines('Original Predictions: ' + repr(pred_write) + '\n') 
file.writelines('Original/Rounded Targets: ' + repr(test_target) + '\n') 
file.writelines('Rounded Predictions: ' + repr(test_predictions) + '\n') 
file.writelines('Step MSE: ' + repr(step_mse) + '\n') 
file.writelines('Step Error: ' + repr(step_error) + '\n') 
file.close()

fig_13, ax6 = plt.subplots(figsize=(5,3))
plt.title('Training Losses')
plt.legend(['Train Loss'], loc='upper right')
plt.plot(step_losses, color='blue')
plt.show(ax6)

fig_14, ax7 = plt.subplots(figsize=(5,3))
plt.title('All Losses')
plt.plot(all_losses)
plt.show(ax7)


