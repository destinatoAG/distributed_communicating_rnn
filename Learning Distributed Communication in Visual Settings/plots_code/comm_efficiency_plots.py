#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 16:20:33 2019

@author: priudu
"""

import pandas as pd 
import matplotlib.pyplot as plt 
import numpy as np

step_mse =  [0.1567878,  0.11274294, 0.08703857, 0.0730486,  0.06206706, 0.05485618,
 0.0506985,  0.04638771, 0.04258767, 0.03981722]

step_mse = np.sqrt(step_mse)

step_error =  [0.48331859, 0.3940251,  0.336712,   0.30221005, 0.27099898, 0.2492898,
 0.23722602, 0.22468237, 0.21244085, 0.20419633]

messages = [17.19, 17.72, 15.92, 15.88, 12.79, 12.13, 11.71, 12.36, 12.43,  9.78]

messages = np.sort(messages)


r200 = [3.51, 3.49,3.68,3.4,3.64]
r200av = np.average(r200)
r200std = np.std(r200)

r400 = [2.93,3.37,2.99,3.18,3.41]
r400av = np.average(r400)
r400std = np.std(r400)

#r600 = [2.51,2.4,2.6,2.3,2.23]
r600 = [2.81,2.7,2.9,2.6,2.53]
r600av = np.average(r600)
r600std = np.std(r600)

r800 = [2.23,2.42,2.66,2.48,2.57]
r800av = np.average(r800)
r800std = np.std(r800)

r1000 = [2.23,1.82,1.9,1.92,1.91]
r1000av = np.average(r1000)
r1000std = np.std(r1000)


epochs = np.arange(5)
elabels = ['1','2','3','4','5']
node_means = [r200av, r400av, r600av, r800av, r1000av]
node_error = [r200std, r400std, r600std, r800std, r1000std]

index = ['Node 1', 'Node 2', 'Node 3',
          'Node 4', 'Node 5']
df = pd.DataFrame({'200': r200, 
                    '400': r400,
                    '600': r600,
                    '800': r800,
                    '1000': r1000}, index=index)


ax = df.plot.barh(stacked='True', figsize=(10,8), fontsize=12).legend(bbox_to_anchor=(1.0, 1.0))
#ax.savefig('C:/Users/priudu/mnist_model/experiments_section/experiments_plots/mnist/nodecomm.png', dpi=ax.dpi)


fig_1a, ax1a = plt.subplots(figsize=(12,8))
ax1a.plot(messages, step_mse, color = 'red', label='RMSE', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(messages,step_error, color='green', label='Error % ', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.legend(loc='upper right', fontsize = 'xx-large')
plt.xlabel('# of messages communicated overall', fontsize=22)
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.grid(True)
plt.show(ax1a)
fig_1a.savefig('figures/comm_msgs.png', dpi=600)

nodes_2_3_5 = [0.171, 0.069, 0.075, 0.050, 0.073, 0.034, 0.043, 0.030, 0.022, 0.021,0.016]
nodes_5_3_2 = [0.175, 0.077, 0.052, 0.054, 0.038, 0.042, 0.057, 0.048, 0.037, 0.039] 
nodes_2_3_5_a = [0.374, 0.167, 0.107, 0.071 ,0.080, 0.051, 0.053, 0.037, 0.032, 0.030, 0.025]

fig_1a, ax1a = plt.subplots(figsize=(12,8))
ax1a.plot(nodes_5_3_2, color = 'orange', label='Nodes 2,3,3 train',  linewidth=2)
ax1a.plot(nodes_2_3_5, color = 'blue', label='Nodes 2,3,3 train',  linewidth=2)
ax1a.plot(nodes_2_3_5_a, color = 'green', label='Nodes 2,3,3 train',  linewidth=2)
plt.show(ax1a)
#fig_1a.savefig('figures/node2_3_5train.png', dpi=fig_1a.dpi)

# Build the plot
fig, ax = plt.subplots(figsize=(10,8))
ax.bar(epochs, node_means, yerr=node_error, align='center', ecolor='black', capsize=10, color='darkorange')
ax.set_ylabel('Avg. number of messages communicated', fontsize=22)
ax.set_xlabel('Epochs', fontsize=22)
ax.set_xticks(epochs)
ax.set_xticklabels(elabels)
plt.setp(ax.get_xticklabels(), fontsize=22)
plt.setp(ax.get_yticklabels(), fontsize=22)
ax.yaxis.grid(True)
plt.tight_layout()
plt.show()
#fig.savefig('figures/nodescomm.png', dpi=fig.dpi)
