# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 20:05:26 2020

@author: destinatoag
"""

import matplotlib.pyplot as plt 
import numpy as np

def plot_stacked_bar(data, series_labels, category_labels=None, 
                     show_values=False, value_format="{}", y_label=None, 
                     colors=None, grid=False, reverse=False):
    
    
    ny = len(data[0])
    ind = list(range(ny))

    axes = []
    cum_size = np.zeros(ny)

    data = np.array(data)

    if reverse:
        data = np.flip(data, axis=1)
        category_labels = reversed(category_labels)

    for i, row_data in enumerate(data):
        axes.append(plt.bar(ind, row_data, bottom=cum_size, 
                            label=series_labels[i], color=colors[i]))
        cum_size += row_data

    if category_labels:
        plt.xticks(ind, category_labels, fontsize='22')

    if y_label:
        plt.ylabel(y_label)

    plt.legend(fontsize='xx-large')

    if grid:
        plt.grid()

    if show_values:
        for axis in axes:
            for bar in axis:
                w, h = bar.get_width(), bar.get_height()
                plt.text(bar.get_x() + w/2, bar.get_y() + h/2, 
                         value_format.format(h), ha="center", 
                         va="center")
 
fig, ax = plt.subplots(figsize=(11,8))

series_labels = ['10 features', '25 features','50 features' ]

data = [
    [32100, 77040, 128400, 256800],
    [54225, 130140, 216900, 433800],
     [91100, 218640, 364400, 728800],
]

category_labels = ['5', '12', '20', '40']

plot_stacked_bar(
    data, 
    series_labels, 
    category_labels=category_labels, 
    show_values=True, 
    value_format="{:.1f}",
    colors=['steelblue', 'goldenrod', 'tab:green' ],
    
)

#plt.title('', fontsize=26)
plt.setp(ax.get_xticklabels(), fontsize=22) 
plt.setp(ax.get_yticklabels(), fontsize=22) 

ticks = ax.get_yticks()*0.001
ticks = ticks.astype(int)
ax.set_yticklabels(ticks)

plt.xlabel('Number of nodes', fontsize=26)
plt.ylabel('# of FLOPs (K)', fontsize=26)
plt.savefig('figures/computation.png', dpi=600)
plt.show(ax)











