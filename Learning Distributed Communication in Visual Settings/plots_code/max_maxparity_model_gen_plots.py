# -*- coding: utf-8 -*-
"""
Created on Mon Sep  2 16:20:33 2019

@author: priudu
"""
import matplotlib.pyplot as plt
import numpy as np 


max_25f_1c_8n_losses = [0.35069859, 0.0918639 , 0.0652362 , 0.04188652, 0.02829784,
       0.02161139, 0.01265589, 0.01562097, 0.01208405, 0.00934499]

parity_25f_1c_8n_losses = [0.47910428, 0.31010666, 0.28861037, 0.31567812, 0.2631107 ,
       0.22642536, 0.23679411, 0.22659573, 0.2454467 , 0.23155499]

max_10f_1c_8n_losses = [0.22066057, 0.09124483, 0.0439299 , 0.02795547, 0.02193522,
       0.02593083, 0.01744424, 0.01556003, 0.01144607, 0.00996352]

parity_10f_1c_8n_losses = [0.542759  , 0.31731778, 0.23765902, 0.27993348, 0.22303043,
       0.25748494, 0.23060617, 0.21601942, 0.21453367, 0.20501207]


max_25f_1c_5n_losses = [0.20826626, 0.09141859, 0.06356902, 0.02547623, 0.0242078 ,
       0.02093432, 0.0208122 , 0.02786629, 0.01740916, 0.01891229]

parity_25f_1c_5n_losses = [0.6295476 , 0.40696502, 0.3223004 , 0.29485977, 0.23865865,
       0.31162503, 0.25859725, 0.23014115, 0.24383709, 0.23306392]


max_10f_1c_5n_losses = [0.18602358, 0.09135942, 0.02813164, 0.02237386, 0.03134532,
       0.02664469, 0.02303348, 0.0191693 , 0.01909757, 0.01629628]


parity_10f_1c_5n_losses = [0.57264495, 0.34069359, 0.40612143, 0.28796843, 0.26506448,
       0.27709705, 0.26952213, 0.23142134, 0.23250458, 0.22792226]


max_25f_1c_2n_losses = [0.19286136, 0.1130383 , 0.06968659, 0.04883951, 0.05544959,
       0.04397515, 0.04897436, 0.0363893 , 0.04163529, 0.04279988]

parity_25f_1c_2n_losses = [0.4761596 , 0.36814904, 0.28066939, 0.26034343, 0.27136496,
       0.26310864, 0.21694401, 0.18749933, 0.21862341, 0.18528304]

max_10f_1c_2n_losses = [0.30502912, 0.17266329, 0.08454431, 0.06396274, 0.05584731,
       0.04371191, 0.03686615, 0.04155659, 0.03981842, 0.04471609]

parity_10f_1c_2n_losses = [0.39770716, 0.26882538, 0.21009219, 0.21119784, 0.2334148 ,
       0.20588979, 0.19461468, 0.22174971, 0.19716291, 0.22020638]

max_25f_1c_8n_error_rate = [0.0947396825396828]
max_25f_1c_8n_rmse = [0.10459445491994307] 
parity_25f_1c_8n_error_rate = [1.7466399999999975]
parity_25f_1c_8n_rmse = [0.46229860479997126] 
max_10f_1c_8n_error_rate = [0.11369206349206389]
max_10f_1c_8n_rmse = [0.11480418110852933] 
parity_10f_1c_8n_error_rate = [1.8330999999999957]
parity_10f_1c_8n_rmse = [0.45635512487535407] 

max_25f_1c_5n_error_rate = [0.17020476190476236]
max_25f_1c_5n_rmse = [0.15665248162732695] 
parity_25f_1c_5n_error_rate = [2.0015599999999992]
parity_25f_1c_5n_rmse = [0.4881188379892749] 
max_10f_1c_5n_error_rate = [0.17514444444444502]
max_10f_1c_5n_rmse = [0.1507978779691545] 
parity_10f_1c_5n_error_rate = [1.7921600000000012]
parity_10f_1c_5n_rmse = [0.46102060691470176] 

max_25f_1c_2n_error_rate = [0.8749055555555558]
max_25f_1c_2n_rmse = [0.19047309521294603] 
parity_25f_1c_2n_error_rate = [1.4615599999999997]
parity_25f_1c_2n_rmse = [0.42121253542600084] 

max_10f_1c_2n_error_rate = [1.0353174603174604]
max_10f_1c_2n_rmse = [0.1956016359849784] 
parity_10f_1c_2n_error_rate = [2.246100000000001]
parity_10f_1c_2n_rmse = [0.45424662904638047]  

scat_error = np.arange(0,0.1,0.1)
steps = np.arange(100,1100,100)

fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('Max-parity task', fontsize=26)
ax1a.plot(steps,parity_25f_1c_2n_losses, label='2 nodes', color='blue')
ax1a.plot(steps,parity_25f_1c_5n_losses, label='5 nodes', color='orange')
ax1a.plot(steps,parity_25f_1c_8n_losses, label='8 nodes', color='green')
#ax1a.set_yticks(np.arange(0,0.7,0.1))
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.xlabel('Samples', fontsize=26)
plt.ylabel('Losses', fontsize=26)
ax1a.legend(loc='upper right', fontsize=26)
plt.show(ax1a)
#fig_1a.savefig('figures/learncomm_parity25f.png', dpi=600)


fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('Max task', fontsize=26)
ax1a.plot(steps,max_25f_1c_2n_losses, label='2 nodes' , color='blue')
ax1a.plot(steps,max_25f_1c_5n_losses, label='5 nodes', color='orange')
ax1a.plot(steps,max_25f_1c_8n_losses, label='8 nodes', color='green')
#ax1a.set_yticks(np.arange(0,0.7,0.1))
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.xlabel('Samples', fontsize=26)
plt.ylabel('Losses', fontsize=26)
ax1a.legend(loc='upper right', fontsize=26)
plt.show(ax1a)
#fig_1a.savefig('figures/learncomm_max25f.png', dpi=600)


fig_1, ax1 = plt.subplots(figsize=(6,4))
plt.title('Training losses (M=1,000, 8 nodes, Max Task, Comm = 1)')
ax1.plot(steps,max_25f_1c_8n_losses, label='25 features, 8 nodes', color='blue')
ax1.plot(steps,max_10f_1c_8n_losses, label='10 features, 8 nodes', color='green')
ax1.legend(loc='upper right')
plt.show(ax1)

fig_1, ax1 = plt.subplots(figsize=(6,4))
plt.title('Training losses (M=1,000, 5 nodes, Max Task, Comm = 1)')
ax1.plot(steps,max_25f_1c_5n_losses, label='25 features, 5 nodes', color='orange')
ax1.plot(steps,max_10f_1c_5n_losses, label='10 features, 5 nodes', color='green')
ax1.legend(loc='upper right')
plt.show(ax1)

fig_1, ax1 = plt.subplots(figsize=(6,4))
plt.title('Training losses (M=1,000, 5 nodes, Max Task, Comm = 1)')
ax1.plot(steps,max_25f_1c_2n_losses, label='25 features, 2 nodes', color='red')
ax1.plot(steps,max_10f_1c_2n_losses, label='10 features, 2 nodes', color='green')
ax1.legend(loc='upper right')
plt.show(ax1)

fig_1, ax1 = plt.subplots(figsize=(6,4))
plt.title('Training losses (M=1,000, 8 nodes, Max Task, Comm = 1)')
ax1.plot(steps,max_25f_1c_8n_losses, label='25 features, 8 nodes', color='blue')
ax1.plot(steps,max_10f_1c_8n_losses, label='10 features, 8 nodes', color='green')
ax1.plot(steps,max_25f_1c_5n_losses, label='25 features, 5 nodes', color='orange')
ax1.plot(steps,max_10f_1c_5n_losses, label='10 features, 5 nodes', color='red')
ax1.plot(steps,max_25f_1c_2n_losses, label='25 features, 2 nodes', color='purple')
ax1.plot(steps,max_10f_1c_2n_losses, label='10 features, 2 nodes', color='magenta')
ax1.legend(loc='upper right')
plt.show(ax1)
#fig_1.savefig('figures/learncomm_all_loses_max.png', dpi=600)


fig_1, ax1 = plt.subplots(figsize=(6,4))
plt.title('Training losses (M=1,000, 5,8 nodes, Parity Task, Comm = 1)')
ax1.plot(steps,parity_25f_1c_8n_losses, label='25 features, 8 nodes', color='blue')
ax1.plot(steps,parity_10f_1c_8n_losses, label='10 features, 8 nodes', color='green')
ax1.plot(steps,parity_25f_1c_5n_losses, label='25 features, 5 nodes', color='orange')
ax1.plot(steps,parity_10f_1c_5n_losses, label='10 features, 5 nodes', color='red')
ax1.plot(steps,parity_25f_1c_2n_losses, label='25 features, 2 nodes', color='purple')
ax1.plot(steps,parity_10f_1c_2n_losses, label='10 features, 2 nodes', color='magenta')
ax1.legend(loc='upper right')
plt.show(ax1)
#fig_1.savefig('figures/learncomm_all_loses_parity.png', dpi=600)


fig_2, ax2 = plt.subplots(figsize=(6,4))
plt.title('Error Rate % (M=1,000, comm = 1)')
ax2.scatter(scat_error, max_25f_1c_8n_error_rate, label='8 node - max, 25f')
ax2.scatter(scat_error, max_10f_1c_8n_error_rate, label='8 node - max, 10f')
ax2.scatter(scat_error, parity_25f_1c_8n_error_rate, label='8 node - max parity, 25f')
ax2.scatter(scat_error, parity_10f_1c_8n_error_rate, label='8 node - max parity, 10f' )
ax2.scatter(scat_error, max_25f_1c_5n_error_rate, label='5 node - max, 25f')
ax2.scatter(scat_error, max_10f_1c_5n_error_rate, label='5 node - max, 10f')
ax2.scatter(scat_error, parity_25f_1c_5n_error_rate, label='5 node - max parity, 25f')
ax2.scatter(scat_error, parity_10f_1c_5n_error_rate, label='5 node - max parity, 10f')
ax2.scatter(scat_error, max_25f_1c_2n_error_rate, label='2 node - max, 25f')
ax2.scatter(scat_error, max_10f_1c_2n_error_rate, label='2 node - max, 10f')
ax2.scatter(scat_error, parity_25f_1c_2n_error_rate, label='2 node - max parity, 25f')
ax2.scatter(scat_error, parity_10f_1c_2n_error_rate, label='2 node - max parity, 10f')

ax2.legend(loc='upper right')
plt.show(ax2)


fig_3, ax2a = plt.subplots(figsize=(6,4))
plt.title('RMSE (M=1,000, comm = 1)')
ax2a.scatter(scat_error, max_25f_1c_8n_rmse, label='8 node - max, 25f')
ax2a.scatter(scat_error, max_10f_1c_8n_rmse, label='8 node - max, 10f')
ax2a.scatter(scat_error, parity_25f_1c_8n_rmse, label='8 node - max parity, 25f')
ax2a.scatter(scat_error, parity_10f_1c_8n_rmse, label='8 node - max parity, 10f')
ax2a.scatter(scat_error, max_25f_1c_5n_rmse, label='5 node - max, 25f')
ax2a.scatter(scat_error, max_10f_1c_5n_rmse, label='5 node - max, 10f')
ax2a.scatter(scat_error, parity_25f_1c_5n_rmse, label='5 node - max parity, 25f')
ax2a.scatter(scat_error, parity_10f_1c_5n_rmse, label='5 node - max parity, 10f')
ax2a.scatter(scat_error, max_25f_1c_2n_rmse, label='2 node - max, 25f')
ax2a.scatter(scat_error, max_10f_1c_2n_rmse, label='2 node - max, 10f')
ax2a.scatter(scat_error, parity_25f_1c_2n_rmse, label='2 node - max parity, 25f')
ax2a.scatter(scat_error, parity_10f_1c_2n_rmse, label='2 node - max parity, 10f')
ax2a.legend(loc='upper right')
plt.show(ax2a)


nodes = [2,5,8]
nodes = np.arange(2,9,3)

#25 features, error rates  
all_error_max_percent = [max_25f_1c_2n_error_rate, max_25f_1c_5n_error_rate, max_25f_1c_8n_error_rate]
all_error_max_rmse = [max_25f_1c_2n_rmse, max_25f_1c_5n_rmse, max_25f_1c_8n_rmse]
all_error_parity_rmse = [parity_25f_1c_2n_rmse, parity_25f_1c_5n_rmse, parity_25f_1c_8n_rmse]
all_error_parity_percent = [parity_25f_1c_2n_error_rate, parity_25f_1c_5n_error_rate, parity_25f_1c_8n_error_rate]

fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('25 features', fontsize=26) 
ax1a.plot(nodes, all_error_max_rmse, color = 'red', label='RMSE - max', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_max_percent, color='green', label='Error % - max', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_parity_rmse, color='blue', label='RMSE - max-parity', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_parity_percent, color='orange', label='Error % - max-parity', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.set_yticks(np.arange(0,3,0.5))
ax1a.legend(loc='upper right', fontsize='xx-large')
plt.ylabel('Accuracy', fontsize=26)
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.xlabel('Number of nodes', fontsize=26)
plt.grid(True)
plt.show(ax1a)
#fig_1a.savefig('figures/learncomm25f.png', dpi=600)

#10 features 
all_error_max_percent = [max_10f_1c_2n_error_rate, max_10f_1c_5n_error_rate ,max_10f_1c_8n_error_rate]
all_error_max_rmse = [max_10f_1c_2n_rmse, max_10f_1c_5n_rmse, max_10f_1c_8n_rmse]
all_error_parity_rmse = [parity_10f_1c_2n_rmse, parity_10f_1c_5n_rmse, parity_10f_1c_8n_rmse]
all_error_parity_percent = [parity_10f_1c_2n_error_rate, parity_10f_1c_5n_error_rate, parity_10f_1c_8n_error_rate]


fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('10 features', fontsize=26) 
ax1a.plot(nodes, all_error_max_rmse, color = 'red', label='RMSE - max', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_max_percent, color='green', label='Error % - max', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_parity_rmse, color='blue', label='RMSE - max-parity', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_parity_percent, color='orange', label='Error % - max-parity', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.set_yticks(np.arange(0,3,0.5))
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
ax1a.legend(loc='upper right', fontsize='xx-large')
plt.ylabel('Accuracy', fontsize=26)
plt.xlabel('Number of nodes', fontsize=26)
plt.grid(True)
plt.show(ax1a)
#fig_1a.savefig('figures/learncomm10f.png', dpi=fig_1a.dpi)


#Additional figures for mod gen section 

import matplotlib.pyplot as plt
import numpy as np  
import matplotlib.patches as mpatches

#Additional figures for mod gen section 

nodes = [6,8,12,16,20,40]
#nodes = np.arange(6,41,6)

max_25f_1c_6n_error_rate = [0.15112222222222274]
max_25f_1c_6n_rmse = [0.13733171520082318] 
max_25f_1c_8n_error_rate = [0.14186825396825437]
max_25f_1c_8n_rmse = [0.141492049246592] 
max_25f_1c_12n_error_rate = [0.11041269841269884]
max_25f_1c_12n_rmse = [0.12173742234826564]
max_25f_1c_16n_error_rate = [0.12742460317460344]
max_25f_1c_16n_rmse = [0.13468481725866507]
max_25f_1c_20n_error_rate = [0.11540873015873067]
max_25f_1c_20n_rmse = [0.12033287165193061]
max_25f_1c_40n_error_rate = [0.11230555555555605]
max_25f_1c_40n_rmse = [0.1210784869413225]



all_error_max_percent = [max_25f_1c_6n_error_rate, max_25f_1c_8n_error_rate, max_25f_1c_12n_error_rate, max_25f_1c_16n_error_rate, max_25f_1c_20n_error_rate, max_25f_1c_40n_error_rate]
all_error_max_rmse = [max_25f_1c_6n_rmse, max_25f_1c_8n_rmse, max_25f_1c_12n_rmse,max_25f_1c_16n_rmse, max_25f_1c_20n_rmse, max_25f_1c_40n_rmse]

y = np.array([0., 0.05, 0.10, 0.15, 0.20, 0.25])


fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('Scaling Network Nodes (25 features)', fontsize=26) 
ax1a.plot(nodes, all_error_max_rmse, color = 'red', label='RMSE - max', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(nodes,all_error_max_percent, color='green', label='Error % - max', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.set_yticks(y)
ax1a.legend(loc='upper right', fontsize='xx-large')
plt.ylabel('Accuracy', fontsize=26)
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.xlabel('Number of nodes', fontsize=26)
plt.grid(True)
plt.show(ax1a)
#fig_1a.savefig('figures/Scaling_25f_6_40.png', dpi=600)

steps1 = np.array([100,500, 700,900,1100, 1200])
steps = np.arange(100,1300,100)
#nodes = np.arange(6,41,6)
max_25f_1c_6n_test_losses = [0.24950257, 0.031294601261615753, 0.021519305184483528, 0.015502274967730045, 0.021724067628383636, 0.01257383730262518]
max_25f_1c_20n_test_losses = [0.24750257, 0.022399435043334961, 0.010995122604072094, 0.011915062554180622, 0.012830580584704876, 0.012530161999166012]



all_losses = [0.259, 0.160, 0.132, 0.097, 0.051, 0.048, 0.046, 0.039, 0.025, 0.026, 0.029, 0.024]
all_losses1 = [0.163, 0.104, 0.083, 0.065, 0.046, 0.031, 0.042, 0.032, 0.020, 0.020, 0.025, 0.020]
#all_losses2 = [0.179,0.093,0.074,0.072,0.051,0.032,0.038,0.035,0.024,0.012,0.018,0.022]
all_losses2 = [0.246,0.112,0.090, 0.063,0.045,0.041,0.041,0.030,0.021,0.022,0.023,0.018]
fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('Dynamic Network Structures', fontsize=26) 
#ax1a.plot(steps, all_losses, color = 'red', label='RMSE - max',  marker='o', markersize=6, linewidth=2)
#ax1a.plot(steps, all_losses1, color = 'green', label='RMSE - max',  marker='o', markersize=6, linewidth=2)
ax1a.plot(steps, all_losses2, color = 'blue', linewidth=2, label='Train 1 Loss')
ax1a.plot(steps, all_losses, color = 'orange', linewidth=2, label='Train 2 Loss')
ax1a.scatter(steps1, max_25f_1c_6n_test_losses, marker='o', s=[50], linestyle='None', label='Test Loss - 20 nodes', color='saddlebrown')
ax1a.scatter(steps1, max_25f_1c_20n_test_losses, marker='o', s=[50], linestyle='None', label='Test Loss - 40 nodes', color='darkblue')
plt.ylabel('Losses', fontsize=26)
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.xlabel('Samples', fontsize=26)
ax1a.axvspan(0, 400, facecolor='indianred', alpha=0.5)
ax1a.axvspan(400, 800, facecolor='steelblue', alpha=0.5)
ax1a.axvspan(800, 1200, facecolor='mediumseagreen', alpha=0.5)
legend2 = plt.legend(loc='upper right', fontsize='xx-large', bbox_to_anchor=(0.935,1.0))
ax1a.add_artist(legend2)
red_patch = mpatches.Patch(color='indianred', label='2 nodes')
blue_patch = mpatches.Patch(color='steelblue', label='3 nodes')
green_patch = mpatches.Patch(color='mediumseagreen', label='5 nodes')
ax1a.legend(handles=[red_patch, blue_patch, green_patch], bbox_to_anchor=(0.933,0.79), fontsize='xx-large')
plt.grid(True)
plt.show(ax1a)
#fig_1a.savefig('figures/learncomm25f_6_40_dynamic.png', dpi=600) 


#Plotting test losses 
steps = np.arange(100,1300,100)
steps1 = np.array([100,500, 700,900,1100, 1200])
#steps2 = np.arange(0,200,100)

max_25f_1c_6n_train_losses = [0.24950257, 0.1543846 , 0.10462963, 0.06665647, 0.06204016,
       0.04578432, 0.03565849, 0.03205773, 0.02957995, 0.02552984,
       0.01488413, 0.0215616]

max_25f_1c_6n_test_losses = [0.24950257, 0.031294601261615753, 0.021519305184483528, 0.015502274967730045, 0.021724067628383636, 0.01257383730262518]
max_25f_1c_20n_test_losses = [0.24750257, 0.022399435043334961, 0.010995122604072094, 0.011915062554180622, 0.012830580584704876, 0.012530161999166012]


fig_1a, ax1a = plt.subplots(figsize=(10,8))
plt.title('40 nodes', fontsize=26)
ax1a.plot(steps,max_25f_1c_6n_train_losses, label='Train Loss', color='blue')
ax1a.scatter(steps1, max_25f_1c_6n_test_losses, marker='o', linestyle='None', label='Test Loss - 20 nodes', color='red')
ax1a.scatter(steps1, max_25f_1c_20n_test_losses, marker='o', linestyle='None', label='Test Loss - 40 nodes', color='orange')
#ax1a.set_yticks(np.arange(0,0.7,0.1))
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.xlabel('Samples', fontsize=26)
plt.ylabel('Losses', fontsize=26)
ax1a.legend(loc='upper right', fontsize=22)
plt.show(ax1a)
#fig_1a.savefig('figures/40nodes_testlosses_max25f.png', dpi=600)

avg6_test_loss = [0.021294601261615753, 0.021519305184483528, 0.015502274967730045, 0.021724067628383636, 0.01257383730262518]
avg8_test_loss = [0.02358098328113556, 0.015533160418272018, 0.020491063594818115, 0.021460743620991707, 0.017484508454799652]
avg12_test_loss = [0.015489207580685616, 0.012394812889397144, 0.016125628724694252, 0.01150943897664547, 0.012921856716275215]
avg16_test_loss = [0.013277542777359486, 0.015020180493593216, 0.014729389920830727, 0.014033764600753784, 0.01347584743052721]
avg20_test_loss = [0.012399435043334961, 0.010995122604072094, 0.011915062554180622, 0.012830580584704876, 0.012530161999166012]
avg40_test_loss = [0.01187902595847845, 0.011986693367362022, 0.012187743559479713, 0.010033955797553062, 0.010620529763400555]

node6_avg = sum(avg6_test_loss) / 5
node8_avg = sum(avg8_test_loss) / 5
node12_avg = sum(avg12_test_loss) / 5
node16_avg = sum(avg16_test_loss) / 5
node20_avg = sum(avg20_test_loss) / 5
node40_avg = sum(avg40_test_loss) / 5

print('6 node avg test loss: ', node6_avg)
print('8 node avg test loss: ', node8_avg)
print('12 node avg test loss: ', node12_avg)
print('16 node avg test loss: ', node16_avg)
print('20 node avg test loss: ', node20_avg)
print('40 node avg test loss: ', node40_avg)
