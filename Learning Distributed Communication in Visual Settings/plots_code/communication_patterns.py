# -*- coding: utf-8 -*-
"""
Created on Fri May  8 12:08:34 2020

@author: destinatoag
"""
import numpy as np 
import matplotlib.pyplot as plt 

data = np.random.randint(2, size=(4, 4))
data1 = np.random.randint(2, size=(4, 4))
data2 = np.random.randint(2, size=(4, 4))
data3 = np.random.randint(2, size=(4, 4))
data4 = np.random.randint(2, size=(4, 4))

print(data.shape)

#from low runs 
data_low_0 = np.asarray([[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]])
data_low_1 = np.asarray([[0,1,0,0],[0,0,1,0],[0,0,0,0],[0,0,0,0]])
data_low_2 = np.asarray([[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,1]])
data_low_3 = np.asarray([[0,0,0,0],[0,0,0,0],[0,1,0,0],[1,0,0,0]])
data_low_4 = np.asarray([[0,0,0,1],[0,0,0,0],[0,0,1,1],[0,0,0,0]])


#from medium runs 
data_medium_0 = np.asarray([[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]])
data_medium_1 = np.asarray([[1,1,0,0],[1,0,0,0],[0,0,0,0],[0,0,0,0]])
data_medium_2 = np.asarray([[0,0,0,0],[0,1,0,1],[1,0,0,1],[0,1,0,0]])
data_medium_3 = np.asarray([[0,0,0,1],[1,1,1,1],[0,0,0,0],[0,0,0,0]])
data_medium_4 = np.asarray([[1,0,0,0],[1,1,1,0],[0,1,1,1],[0,1,0,0]])

#from high runs 
data_high_0 = np.asarray([[1,1,1,1],[1,1,1,1],[1,1,1,1],[1,1,1,1]])
data_high_1 = np.asarray([[0,0,0,0],[1,1,0,1],[0,0,0,0],[0,0,0,0]])
data_high_2 = np.asarray([[1,0,1,0],[1,0,0,0],[0,1,0,1],[0,1,0,1]])
data_high_3 = np.asarray([[0,0,0,1],[0,1,1,1],[0,0,0,1],[1,1,0,1]])
data_high_4 = np.asarray([[0,0,1,0],[1,1,0,1],[1,0,1,1],[1,1,1,1]])

fig, axs = plt.subplots(1, 5, figsize=(12, 6))
axs[0].matshow(data_low_0, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data_low_1,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data_low_2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data_low_3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data_low_4,cmap=plt.cm.summer, extent=[0,4,0,4])

axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)

axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))

plt.show()


fig, axs = plt.subplots(1, 5, figsize=(12, 6))
axs[0].matshow(data_medium_0, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data_medium_1,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data_medium_2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data_medium_3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data_medium_4,cmap=plt.cm.summer, extent=[0,4,0,4])

axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)

axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))


plt.show()


fig1, axs = plt.subplots(1, 5, figsize=(12, 6))
axs[0].matshow(data_high_0, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data_high_1,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data_high_2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data_high_3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data_high_4,cmap=plt.cm.summer, extent=[0,4,0,4])

axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)

axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))


plt.show()



