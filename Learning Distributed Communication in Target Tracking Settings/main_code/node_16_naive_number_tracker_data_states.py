# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 12:41:10 2020

@author: destinatoag
"""

import torch
import numpy as np 
from adjacency_matrix import adj_matrix
from torch.autograd import Variable 

g = { "1" : ["2", "16"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "6"],
          "6" : ["5","7"],
          "7" : ["6","8"],
          "8" : ["7","9"],
          "9" : ["8", "10"],
          "10" : ["9", "11"],
          "11" : ["10","12"],
          "12" : ["11", "13"],
          "13" : ["12", "14"],
          "14" : ["13","15"],
          "15" : ["14","16"],
          "16" : ["15","1"]
          
        }

grid_size = len(g)
tracking_steps = 8
sensor_in = 1
hidden = 2
samples = 150
tracked_digit = 1

net_config = adj_matrix(g)[0]
net_matrix = adj_matrix(g)[1] 

net_matrix_torch = torch.autograd.Variable(torch.from_numpy(net_matrix)).float()

class Grid():
    def __init__(self):
        super(Grid, self).__init__()
        #self.grid = torch.empty(3,3).random_(2) 
        self.grid = torch.Tensor(4,4).random_(3,10)
        self.grid[0][0] = tracked_digit 
        self.original_grid = self.grid 
        
        #variables only valid for a 16x16 grid, with no diagonal movements  
        self.x = 1
        self.y = 4
        #self.z = 4
        self.tracking_init = 0
        self.tracking_init_1 = 0
        self.tracking_pos = 0
   
    def shift(self, grid_size): 
        row1 = 0
        row2 = 1
        row3 = 2
        row4 = 3
        rows = np.sqrt(grid_size)
    
        
        #Initial choice
        #First move always shifts position, we can't choose original position 
        move_position = np.random.choice([self.x,self.y]) 
        g = self.original_grid.unsqueeze(0) 
        grids_list = g.clone()
        factor = int(grid_size / rows)
        
        for x in range(tracking_steps-1):
        
            revert_grid = self.grid.clone()
            
            if move_position > 3 and move_position < 8: 
                self.grid[row2][move_position-factor] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row2][move_position-factor]
                self.tracking_init = row2
                self.tracking_init_1 = move_position-factor
                
                if move_position == 4:
                    move_position = np.random.choice([0,4,5,8])
                elif move_position == 5: 
                    move_position = np.random.choice([1,4,5,6,9])
                elif move_position == 6: 
                    move_position = np.random.choice([2,6,7,5,10])
                else:
                    move_position = np.random.choice([3,6,7,11])
                    
            elif move_position > 7 and move_position < 12: 
                self.grid[row3][move_position-(factor*2)] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row3][move_position-(factor*2)]
                self.tracking_init = row3
                self.tracking_init_1 = move_position-(factor*2)
                
                if move_position == 8:
                    move_position = np.random.choice([4,12,9,8])
                elif move_position == 9: 
                    move_position = np.random.choice([9,5,8,10,13])
                elif move_position == 10: 
                    move_position = np.random.choice([6,9,10,14,11])
                else:
                    move_position = np.random.choice([11,7,10,15])
                    
            elif move_position > 11: 
                self.grid[row4][move_position-(factor*3)] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row4][move_position-(factor*3)]
                self.tracking_init = row4
                self.tracking_init_1 = move_position-(factor*3)
                
                if move_position == 12:
                    move_position = np.random.choice([8,12,13])
                elif move_position == 13: 
                    move_position = np.random.choice([13,12,9,14])
                elif move_position == 14: 
                    move_position = np.random.choice([14,13,15,10])
                else:
                    move_position = np.random.choice([15,14,11])
                
            else:
                self.grid[row1][move_position] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row1][move_position]
                self.tracking_init = row1
                self.tracking_init_1 = move_position
                
                if move_position == 0:
                    move_position = np.random.choice([1,0,4])
                elif move_position == 1: 
                    move_position = np.random.choice([2,1,0,5])
                elif move_position == 2: 
                    move_position = np.random.choice([1,3,6,2])
                else:
                    move_position = np.random.choice([2,7,3])
            
            current_grid = self.grid.unsqueeze(0)
            current_grid = current_grid.clone()
            
            grids_list = torch.cat((grids_list, current_grid)) 
   
        return grids_list
    

def node_data():
    
    grid_of_grids = torch.tensor([])
    
    node1_data = torch.FloatTensor([])
    node2_data = torch.FloatTensor([])
    node3_data = torch.FloatTensor([])
    node4_data = torch.FloatTensor([])
    node5_data = torch.FloatTensor([])
    node6_data = torch.FloatTensor([])
    node7_data = torch.FloatTensor([])
    node8_data = torch.FloatTensor([])
    node9_data = torch.FloatTensor([])
    node10_data = torch.FloatTensor([])
    node11_data = torch.FloatTensor([])
    node12_data = torch.FloatTensor([])
    node13_data = torch.FloatTensor([])
    node14_data = torch.FloatTensor([])
    node15_data = torch.FloatTensor([])
    node16_data = torch.FloatTensor([])
    
    for j in range(samples): 
        data = Grid.shift(Grid(), grid_size)
       
        grid_of_grids = torch.cat((grid_of_grids, data)) 
      
    #Number of samples 
        for k in range(tracking_steps):
      
            #k is grid number, seconf [] is row number, third is node position 
            node1_data = torch.cat((node1_data, data[k][0][0].unsqueeze(0)))
            node2_data = torch.cat((node2_data, data[k][0][1].unsqueeze(0))) 
            node3_data = torch.cat((node3_data, data[k][0][2].unsqueeze(0)))
            node4_data = torch.cat((node4_data, data[k][0][3].unsqueeze(0)))
            node5_data = torch.cat((node5_data, data[k][1][0].unsqueeze(0)))
            node6_data = torch.cat((node6_data, data[k][1][1].unsqueeze(0)))
            node7_data = torch.cat((node7_data, data[k][1][2].unsqueeze(0)))
            node8_data = torch.cat((node8_data, data[k][1][3].unsqueeze(0)))
            node9_data = torch.cat((node9_data, data[k][2][0].unsqueeze(0)))
            node10_data = torch.cat((node10_data, data[k][2][1].unsqueeze(0)))
            node11_data = torch.cat((node11_data, data[k][2][2].unsqueeze(0)))
            node12_data = torch.cat((node12_data, data[k][2][3].unsqueeze(0)))
            node13_data = torch.cat((node13_data, data[k][3][0].unsqueeze(0)))
            node14_data = torch.cat((node14_data, data[k][3][1].unsqueeze(0)))
            node15_data = torch.cat((node15_data, data[k][3][2].unsqueeze(0)))
            node16_data = torch.cat((node16_data, data[k][3][3].unsqueeze(0)))
            
    return node1_data, node2_data, node3_data, node4_data, node5_data, node6_data, node7_data, node8_data, node9_data, node10_data, node11_data, node12_data, node13_data, node14_data, node15_data, node16_data, grid_of_grids

def states_pool(num_nodes, state_size): 
    py_init_states = Variable(torch.zeros(grid_size, state_size))
    cam_pos = 3 
    #Turn on all nodes initially 
    for k in range(num_nodes): 
        py_init_states[k][cam_pos] = 1.0  
        
    return py_init_states


