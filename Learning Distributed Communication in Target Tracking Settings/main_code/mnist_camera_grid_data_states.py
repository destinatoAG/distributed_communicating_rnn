# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 02:14:17 2020

@author: destinatoag
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jan 30 12:41:10 2020

@author: destinatoag
"""

import torch
import numpy as np 
import torch.nn as nn
import torch.nn.functional as F
from adjacency_matrix import adj_matrix
from torch.autograd import Variable 
from mnist_templates import mnist_digits

mnist_digits_data = mnist_digits()

feature_space_size = 25
sensor_in = 25

g = { "1" : ["2", "16"],
          "2" : ["1", "3"],
          "3" : ["2","4"],
          "4" : ["3", "5"],
          "5" : ["4", "6"],
          "6" : ["5","7"],
          "7" : ["6","8"],
          "8" : ["7","9"],
          "9" : ["8", "10"],
          "10" : ["9", "11"],
          "11" : ["10","12"],
          "12" : ["11", "13"],
          "13" : ["12", "14"],
          "14" : ["13","15"],
          "15" : ["14","16"],
          "16" : ["15","1"]
          
        }

grid_size = len(g)
tracking_steps = 8
hidden = 2
samples = 150
tracked_digit = 1
target_size = 1

net_config = adj_matrix(g)[0]
net_matrix = adj_matrix(g)[1] 

net_matrix_torch = torch.autograd.Variable(torch.from_numpy(net_matrix)).float()

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(1, 10, kernel_size=5)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)
        self.conv2_drop = nn.Dropout2d()
        self.fc1 = nn.Linear(320, 50)
        self.fc2 = nn.Linear(50, feature_space_size)
        self.fc3 = nn.Linear(feature_space_size,10)
     
    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, 320)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        exit_node_comm = x
    
        return exit_node_comm 

class Grid():
    def __init__(self):
        super(Grid, self).__init__()
        #self.grid = torch.empty(3,3).random_(2) 
        self.grid = torch.Tensor(4,4).random_(3,10)
        self.grid[0][0] = tracked_digit 
        self.original_grid = self.grid 
        
        #variables only valid for a 16x16 grid, with no diagonal movements  
        self.x = 1
        self.y = 4
        #self.z = 4
        self.tracking_init = 0
        self.tracking_init_1 = 0
        self.tracking_pos = 0
   
    def shift(self, grid_size): 
        row1 = 0
        row2 = 1
        row3 = 2
        row4 = 3
        rows = np.sqrt(grid_size)
    
        
        #Initial choice
        #First move always shifts position, we can't choose original position 
        move_position = np.random.choice([self.x,self.y]) 
        g = self.original_grid.unsqueeze(0) 
        grids_list = g.clone()
        factor = int(grid_size / rows)
        
        for x in range(tracking_steps-1):
        
            revert_grid = self.grid.clone()
            
            if move_position > 3 and move_position < 8: 
                self.grid[row2][move_position-factor] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row2][move_position-factor]
                self.tracking_init = row2
                self.tracking_init_1 = move_position-factor
                
                if move_position == 4:
                    move_position = np.random.choice([0,4,5,8])
                elif move_position == 5: 
                    move_position = np.random.choice([1,4,5,6,9])
                elif move_position == 6: 
                    move_position = np.random.choice([2,6,7,5,10])
                else:
                    move_position = np.random.choice([3,6,7,11])
                    
            elif move_position > 7 and move_position < 12: 
                self.grid[row3][move_position-(factor*2)] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row3][move_position-(factor*2)]
                self.tracking_init = row3
                self.tracking_init_1 = move_position-(factor*2)
                
                if move_position == 8:
                    move_position = np.random.choice([4,12,9,8])
                elif move_position == 9: 
                    move_position = np.random.choice([9,5,8,10,13])
                elif move_position == 10: 
                    move_position = np.random.choice([6,9,10,14,11])
                else:
                    move_position = np.random.choice([11,7,10,15])
                    
            elif move_position > 11: 
                self.grid[row4][move_position-(factor*3)] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row4][move_position-(factor*3)]
                self.tracking_init = row4
                self.tracking_init_1 = move_position-(factor*3)
                
                if move_position == 12:
                    move_position = np.random.choice([8,12,13])
                elif move_position == 13: 
                    move_position = np.random.choice([13,12,9,14])
                elif move_position == 14: 
                    move_position = np.random.choice([14,13,15,10])
                else:
                    move_position = np.random.choice([15,14,11])
                
            else:
                self.grid[row1][move_position] = self.grid[self.tracking_init][self.tracking_init_1] 
                self.grid[self.tracking_init][self.tracking_init_1] = revert_grid[row1][move_position]
                self.tracking_init = row1
                self.tracking_init_1 = move_position
                
                if move_position == 0:
                    move_position = np.random.choice([1,0,4])
                elif move_position == 1: 
                    move_position = np.random.choice([2,1,0,5])
                elif move_position == 2: 
                    move_position = np.random.choice([1,3,6,2])
                else:
                    move_position = np.random.choice([2,7,3])
            
            current_grid = self.grid.unsqueeze(0)
            current_grid = current_grid.clone()
            
            grids_list = torch.cat((grids_list, current_grid)) 
            
        return grids_list
       
class Node(Grid, Net):
    def __init__(self):
        super(Node, self).__init__()
        
        self.grid_of_grids = torch.tensor([])
        self.node1_data = torch.FloatTensor([])
        self.node2_data = torch.FloatTensor([])
        self.node3_data = torch.FloatTensor([])
        self.node4_data = torch.FloatTensor([])
        self.node5_data = torch.FloatTensor([])
        self.node6_data = torch.FloatTensor([])
        self.node7_data = torch.FloatTensor([])
        self.node8_data = torch.FloatTensor([])
        self.node9_data = torch.FloatTensor([])
        self.node10_data = torch.FloatTensor([])
        self.node11_data = torch.FloatTensor([])
        self.node12_data = torch.FloatTensor([])
        self.node13_data = torch.FloatTensor([])
        self.node14_data = torch.FloatTensor([])
        self.node15_data = torch.FloatTensor([])
        self.node16_data = torch.FloatTensor([])
        
        self.data1 = mnist_digits_data[0]
        self.target1 = mnist_digits_data[1]
        
        self.data2 = mnist_digits_data[2]
        self.target2 = mnist_digits_data[3]
        
        self.data3 = mnist_digits_data[4]
        self.target3 = mnist_digits_data[5]
        
        self.data4 = mnist_digits_data[6]
        self.target4 = mnist_digits_data[7]
        
        self.data5 = mnist_digits_data[8]
        self.target5 = mnist_digits_data[9]
        
        self.data6 = mnist_digits_data[10]
        self.target6 = mnist_digits_data[11]
        
        self.data7 = mnist_digits_data[12]
        self.target7 = mnist_digits_data[13]
             
        self.data8 = mnist_digits_data[14]
        self.target8 = mnist_digits_data[15]
        
        self.data9 = mnist_digits_data[16]
        self.target9 = mnist_digits_data[17]
        
        self.node1_data_in = torch.FloatTensor([])
        self.node2_data_in = torch.FloatTensor([])
        self.node3_data_in = torch.FloatTensor([])
        self.node4_data_in = torch.FloatTensor([])
        self.node5_data_in = torch.FloatTensor([])
        self.node6_data_in = torch.FloatTensor([])
        self.node7_data_in = torch.FloatTensor([])
        self.node8_data_in = torch.FloatTensor([])
        self.node9_data_in = torch.FloatTensor([])
        self.node10_data_in = torch.FloatTensor([])
        self.node11_data_in = torch.FloatTensor([])
        self.node12_data_in = torch.FloatTensor([])
        self.node13_data_in = torch.FloatTensor([])
        self.node14_data_in = torch.FloatTensor([])
        self.node15_data_in = torch.FloatTensor([])
        self.node16_data_in = torch.FloatTensor([])
        
        self.nodes_list_data = torch.tensor([])
        
        continued_network = Net()
        network_state_dict = torch.load('../models/25f_1c_16n_model.pth')
        continued_network.load_state_dict(network_state_dict)
        
        self.continued_network = continued_network
         
    def node_data(self):
        
        for j in range(samples): 
            data = Grid.shift(Grid(), grid_size)
            #print(data.shape) 
            self.grid_of_grids = torch.cat((self.grid_of_grids, data)) 
            #print(grid_of_grids.shape)
            
        #Number of samples 
            for k in range(tracking_steps):
                
                #k is grid number, second [] is row number, third is node position 
                self.node1_data = torch.cat((self.node1_data, data[k][0][0].unsqueeze(0)))
                self.node2_data = torch.cat((self.node2_data, data[k][0][1].unsqueeze(0))) 
                self.node3_data = torch.cat((self.node3_data, data[k][0][2].unsqueeze(0)))
                self.node4_data = torch.cat((self.node4_data, data[k][0][3].unsqueeze(0)))
                self.node5_data = torch.cat((self.node5_data, data[k][1][0].unsqueeze(0)))
                self.node6_data = torch.cat((self.node6_data, data[k][1][1].unsqueeze(0)))
                self.node7_data = torch.cat((self.node7_data, data[k][1][2].unsqueeze(0)))
                self.node8_data = torch.cat((self.node8_data, data[k][1][3].unsqueeze(0)))
                self.node9_data = torch.cat((self.node9_data, data[k][2][0].unsqueeze(0)))
                self.node10_data = torch.cat((self.node10_data, data[k][2][1].unsqueeze(0)))
                self.node11_data = torch.cat((self.node11_data, data[k][2][2].unsqueeze(0)))
                self.node12_data = torch.cat((self.node12_data, data[k][2][3].unsqueeze(0)))
                self.node13_data = torch.cat((self.node13_data, data[k][3][0].unsqueeze(0)))
                self.node14_data = torch.cat((self.node14_data, data[k][3][1].unsqueeze(0)))
                self.node15_data = torch.cat((self.node15_data, data[k][3][2].unsqueeze(0)))
                self.node16_data = torch.cat((self.node16_data, data[k][3][3].unsqueeze(0)))
 
        tensor_list = [self.node1_data, self.node2_data, self.node3_data, self.node4_data, self.node5_data, self.node6_data, self.node7_data, self.node8_data, self.node9_data, self.node10_data, self.node11_data, self.node12_data, self.node13_data, self.node14_data, self.node15_data, self.node16_data]
        
        self.nodes_list_data = torch.stack(tensor_list)
        
        for x in range(tracking_steps*samples):
            if self.node1_data[x] == 1:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data1))  
            if self.node1_data[x] == 3:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data3))  
            if self.node1_data[x] == 4:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data4)) 
            if self.node1_data[x] == 5:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data5)) 
            if self.node1_data[x] == 6:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data6))   
            if self.node1_data[x] == 7:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data7))  
            if self.node1_data[x] == 8:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data8))
            if self.node1_data[x] == 9:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data9))
                
        node_mnist_data = []
        all_mnist_data = []
  
        for i in range(grid_size): 
            for j in range(tracking_steps*samples): 
                if self.nodes_list_data[i][j] == 1.0: 
                    node_mnist_data.append(self.data1)
                if self.nodes_list_data[i][j] == 2.0:
                    node_mnist_data.append(self.data2)
                if self.nodes_list_data[i][j] == 3.0:
                    node_mnist_data.append(self.data3)
                if self.nodes_list_data[i][j] == 4.0:
                    node_mnist_data.append(self.data4)
                if self.nodes_list_data[i][j] == 5.0:
                    node_mnist_data.append(self.data5)
                if self.nodes_list_data[i][j] == 6.0:
                    node_mnist_data.append(self.data6)
                if self.nodes_list_data[i][j] == 7.0:
                    node_mnist_data.append(self.data7)
                if self.nodes_list_data[i][j] == 8.0:
                    node_mnist_data.append(self.data8)
                if self.nodes_list_data[i][j] == 9.0:
                    node_mnist_data.append(self.data9)
                    
            node_data = torch.stack(node_mnist_data)
            
            all_mnist_data.append(node_data)    
            
            node_mnist_data = []
            
        all_nodes_mnist_data = torch.stack(all_mnist_data) 
      
        node1_features = self.continued_network(all_nodes_mnist_data[0])
        node2_features = self.continued_network(all_nodes_mnist_data[1])
        node3_features = self.continued_network(all_nodes_mnist_data[2])
        node4_features = self.continued_network(all_nodes_mnist_data[3])
        node5_features = self.continued_network(all_nodes_mnist_data[4])
        node6_features = self.continued_network(all_nodes_mnist_data[5])
        node7_features = self.continued_network(all_nodes_mnist_data[6])
        node8_features = self.continued_network(all_nodes_mnist_data[7])
        node9_features = self.continued_network(all_nodes_mnist_data[8])
        node10_features = self.continued_network(all_nodes_mnist_data[9])
        node11_features = self.continued_network(all_nodes_mnist_data[10])
        node12_features = self.continued_network(all_nodes_mnist_data[11])
        node13_features = self.continued_network(all_nodes_mnist_data[12])
        node14_features = self.continued_network(all_nodes_mnist_data[13])
        node15_features = self.continued_network(all_nodes_mnist_data[14])
        node16_features = self.continued_network(all_nodes_mnist_data[15])
        grid_of_grids = self.grid_of_grids
        
        all_node_data = [self.node1_data, self.node2_data, self.node3_data, self.node4_data, self.node5_data, self.node6_data, self.node7_data, self.node8_data, self.node9_data, self.node10_data, self.node11_data, self.node12_data, self.node13_data, self.node14_data, self.node15_data, self.node16_data]
        
        return node1_features, node2_features, node3_features, node4_features, node5_features, node6_features, node7_features, node8_features, node9_features, node10_features, node11_features, node12_features, node13_features, node14_features, node15_features, node16_features, grid_of_grids, all_node_data
    

    def test_data(self):
        
        for j in range(samples): 
            data = Grid.shift(Grid(), grid_size)
       
            self.grid_of_grids = torch.cat((self.grid_of_grids, data)) 
            
            #Number of samples 
            for k in range(tracking_steps):
                
                #k is grid number, second [] is row number, third is node position 
                self.node1_data = torch.cat((self.node1_data, data[k][0][0].unsqueeze(0)))
                self.node2_data = torch.cat((self.node2_data, data[k][0][1].unsqueeze(0))) 
                self.node3_data = torch.cat((self.node3_data, data[k][0][2].unsqueeze(0)))
                self.node4_data = torch.cat((self.node4_data, data[k][0][3].unsqueeze(0)))
                self.node5_data = torch.cat((self.node5_data, data[k][1][0].unsqueeze(0)))
                self.node6_data = torch.cat((self.node6_data, data[k][1][1].unsqueeze(0)))
                self.node7_data = torch.cat((self.node7_data, data[k][1][2].unsqueeze(0)))
                self.node8_data = torch.cat((self.node8_data, data[k][1][3].unsqueeze(0)))
                self.node9_data = torch.cat((self.node9_data, data[k][2][0].unsqueeze(0)))
                self.node10_data = torch.cat((self.node10_data, data[k][2][1].unsqueeze(0)))
                self.node11_data = torch.cat((self.node11_data, data[k][2][2].unsqueeze(0)))
                self.node12_data = torch.cat((self.node12_data, data[k][2][3].unsqueeze(0)))
                self.node13_data = torch.cat((self.node13_data, data[k][3][0].unsqueeze(0)))
                self.node14_data = torch.cat((self.node14_data, data[k][3][1].unsqueeze(0)))
                self.node15_data = torch.cat((self.node15_data, data[k][3][2].unsqueeze(0)))
                self.node16_data = torch.cat((self.node16_data, data[k][3][3].unsqueeze(0)))
 
        tensor_list = [self.node1_data, self.node2_data, self.node3_data, self.node4_data, self.node5_data, self.node6_data, self.node7_data, self.node8_data, self.node9_data, self.node10_data, self.node11_data, self.node12_data, self.node13_data, self.node14_data, self.node15_data, self.node16_data]
        
        self.nodes_list_data = torch.stack(tensor_list)
        
        for x in range(tracking_steps*samples):
            if self.node1_data[x] == 1:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data1))  
            if self.node1_data[x] == 3:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data3))  
            if self.node1_data[x] == 4:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data4)) 
            if self.node1_data[x] == 5:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data5)) 
            if self.node1_data[x] == 6:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data6))   
            if self.node1_data[x] == 7:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data7))  
            if self.node1_data[x] == 8:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data8))
            if self.node1_data[x] == 9:
                self.node1_data_in = torch.cat((self.node1_data_in, self.data9))
        
        node_mnist_data = []
        all_mnist_data = []
  
        for i in range(grid_size): 
            for j in range(tracking_steps*samples): 
                if self.nodes_list_data[i][j] == 1.0: 
                    node_mnist_data.append(self.data1)
                if self.nodes_list_data[i][j] == 2.0:
                    node_mnist_data.append(self.data2)
                if self.nodes_list_data[i][j] == 3.0:
                    node_mnist_data.append(self.data3)
                if self.nodes_list_data[i][j] == 4.0:
                    node_mnist_data.append(self.data4)
                if self.nodes_list_data[i][j] == 5.0:
                    node_mnist_data.append(self.data5)
                if self.nodes_list_data[i][j] == 6.0:
                    node_mnist_data.append(self.data6)
                if self.nodes_list_data[i][j] == 7.0:
                    node_mnist_data.append(self.data7)
                if self.nodes_list_data[i][j] == 8.0:
                    node_mnist_data.append(self.data8)
                if self.nodes_list_data[i][j] == 9.0:
                    node_mnist_data.append(self.data9)
                    
            node_data = torch.stack(node_mnist_data)
            
            all_mnist_data.append(node_data)    
            
            node_mnist_data = []
            
        all_nodes_mnist_data = torch.stack(all_mnist_data) 
      
        node1_features = self.continued_network(all_nodes_mnist_data[0])
        node2_features = self.continued_network(all_nodes_mnist_data[1])
        node3_features = self.continued_network(all_nodes_mnist_data[2])
        node4_features = self.continued_network(all_nodes_mnist_data[3])
        node5_features = self.continued_network(all_nodes_mnist_data[4])
        node6_features = self.continued_network(all_nodes_mnist_data[5])
        node7_features = self.continued_network(all_nodes_mnist_data[6])
        node8_features = self.continued_network(all_nodes_mnist_data[7])
        node9_features = self.continued_network(all_nodes_mnist_data[8])
        node10_features = self.continued_network(all_nodes_mnist_data[9])
        node11_features = self.continued_network(all_nodes_mnist_data[10])
        node12_features = self.continued_network(all_nodes_mnist_data[11])
        node13_features = self.continued_network(all_nodes_mnist_data[12])
        node14_features = self.continued_network(all_nodes_mnist_data[13])
        node15_features = self.continued_network(all_nodes_mnist_data[14])
        node16_features = self.continued_network(all_nodes_mnist_data[15])
        grid_of_grids = self.grid_of_grids
        
        all_node_data = [self.node1_data, self.node2_data, self.node3_data, self.node4_data, self.node5_data, self.node6_data, self.node7_data, self.node8_data, self.node9_data, self.node10_data, self.node11_data, self.node12_data, self.node13_data, self.node14_data, self.node15_data, self.node16_data]
        
        return node1_features, node2_features, node3_features, node4_features, node5_features, node6_features, node7_features, node8_features, node9_features, node10_features, node11_features, node12_features, node13_features, node14_features, node15_features, node16_features, grid_of_grids, all_node_data

def states_pool(num_nodes, state_size): 
    py_init_states = Variable(torch.zeros(grid_size, state_size))
    cam_pos = 12
    #Turn on all nodes initially 
    for k in range(num_nodes): 
        py_init_states[k][cam_pos] = 1.0  

    return py_init_states



