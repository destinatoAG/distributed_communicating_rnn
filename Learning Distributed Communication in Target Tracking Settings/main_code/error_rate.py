import numpy as np 

def error_rate(output, target): 
    a = output - target
    a = abs(a)
    
    for i in range(len(target)):
        if target[i] == 0.0 :
            target[i] = 0.01
    a = a / target  
    a = sum(a) / len(target)
    return a
        
