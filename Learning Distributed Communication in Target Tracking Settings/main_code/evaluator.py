#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 20 18:09:17 2021

@author: pabudu
"""
import torch
import numpy as np
import torch.nn as nn
from math import sqrt
from commloss_repo import CommLoss 
from error_rate import error_rate
from sklearn.metrics import mean_squared_error

def normalize(data):
    maxt = torch.max(data)
    x_normed = data / maxt
    return x_normed 

def energy_loss(energy_total, grid_size):
    loss = (energy_total / grid_size)  
    return loss 

def test(net, all_test_data, test_len, states_pool, py_states, input_len, sensor_in, node_input_scalar, tracking_steps, grid_size, state_size):
    #model in eval mode 
    net.eval()
    
    test_predictions = np.asarray([])
    test_targets = np.asarray([])
    pred_write = np.asarray([])
    step_mse = np.asarray([])
    step_error = np.asarray([])
    running_loss = 0.0
    running_comm_loss = 0.0
    step_losses = np.asarray([])
    step_comm_losses = np.asarray([])
    total_comms = 0.0

    total_comms_list = np.asarray([])

    sum_energy_totals = np.asarray([])
    total_node_count = np.asarray([0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
      
    all_losses = np.asarray([])
    energy_totals = np.asarray([])
    sum_energy_totals = np.asarray([])
    all_nodes_on = np.asarray([])
    all_comms_on = np.asarray([])
    total_network_out = torch.tensor([])
    
    accuracy_criterion_1 = nn.MSELoss()
    
    node1_inputs = all_test_data[0]
    node2_inputs = all_test_data[1]
    node3_inputs = all_test_data[2]
    node4_inputs = all_test_data[3]
    node5_inputs = all_test_data[4]
    node6_inputs = all_test_data[5]
    node7_inputs = all_test_data[6]
    node8_inputs = all_test_data[7]
    node9_inputs = all_test_data[8]
    node10_inputs = all_test_data[9]
    node11_inputs = all_test_data[10]
    node12_inputs = all_test_data[11]
    node13_inputs = all_test_data[12]
    node14_inputs = all_test_data[13]
    node15_inputs = all_test_data[14]
    node16_inputs = all_test_data[15]
    
    node1_inputs_scalar = node_input_scalar[0]
    node2_inputs_scalar = node_input_scalar[1]
    node3_inputs_scalar = node_input_scalar[2]
    node4_inputs_scalar = node_input_scalar[3]
    node5_inputs_scalar = node_input_scalar[4]
    node6_inputs_scalar = node_input_scalar[5]
    node7_inputs_scalar = node_input_scalar[6]
    node8_inputs_scalar = node_input_scalar[7]
    node9_inputs_scalar = node_input_scalar[8]
    node10_inputs_scalar = node_input_scalar[9]
    node11_inputs_scalar = node_input_scalar[10]
    node12_inputs_scalar = node_input_scalar[11]
    node13_inputs_scalar = node_input_scalar[12]
    node14_inputs_scalar = node_input_scalar[13]
    node15_inputs_scalar = node_input_scalar[14]
    node16_inputs_scalar = node_input_scalar[15]
    
    #set the requires_grad flag to false as we are in the test mode
    with torch.no_grad():
        
        for k in range(test_len):
            
            if k == input_len - 1: 
                break
     
            input_data = torch.tensor([])
            targets = torch.tensor([])
    
            input_data = torch.cat((input_data, node1_inputs[k].view(1,sensor_in)), 0) 
            input_data = torch.cat((input_data, node2_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node3_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node4_inputs[k].view(1,sensor_in)), 0) 
            input_data = torch.cat((input_data, node5_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node6_inputs[k].view(1,sensor_in)), 0) 
            input_data = torch.cat((input_data, node7_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node8_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node9_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node10_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node11_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node12_inputs[k].view(1,sensor_in)), 0) 
            input_data = torch.cat((input_data, node13_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node14_inputs[k].view(1,sensor_in)), 0) 
            input_data = torch.cat((input_data, node15_inputs[k].view(1,sensor_in)), 0)
            input_data = torch.cat((input_data, node16_inputs[k].view(1,sensor_in)), 0)
    
            input_data_scalar = [node1_inputs_scalar[k].view(1), node2_inputs_scalar[k].view(1), node3_inputs_scalar[k].view(1), node4_inputs_scalar[k].view(1), node5_inputs_scalar[k].view(1), node6_inputs_scalar[k].view(1), node7_inputs_scalar[k].view(1), node8_inputs_scalar[k].view(1), node9_inputs_scalar[k].view(1), node10_inputs_scalar[k].view(1), node11_inputs_scalar[k].view(1), node12_inputs_scalar[k].view(1), node13_inputs_scalar[k].view(1), node14_inputs_scalar[k].view(1), node15_inputs_scalar[k].view(1), node16_inputs_scalar[k].view(1)]
    
            input_data_scalar = torch.stack(input_data_scalar)
    
            targets = [node1_inputs_scalar[k+1].view(1), node2_inputs_scalar[k+1].view(1), node3_inputs_scalar[k+1].view(1), node4_inputs_scalar[k+1].view(1), node5_inputs_scalar[k+1].view(1), node6_inputs_scalar[k+1].view(1), node7_inputs_scalar[k+1].view(1), node8_inputs_scalar[k+1].view(1), node9_inputs_scalar[k+1].view(1), node10_inputs_scalar[k+1].view(1), node11_inputs_scalar[k+1].view(1), node12_inputs_scalar[k+1].view(1), node13_inputs_scalar[k+1].view(1), node14_inputs_scalar[k+1].view(1), node15_inputs_scalar[k+1].view(1), node16_inputs_scalar[k+1].view(1)]
    
            targets = torch.stack(targets)
            
            if k % tracking_steps == 0.0: 
                py_states = states_pool(grid_size, state_size)
        
            targets = targets.view(-1)
       
            output, comms_count, all_node_count, nodes_on, comms_on = net(input_data, input_data_scalar, py_states)
            
            nodes_on = nodes_on.detach().numpy()
            comms_on = comms_on.detach().numpy()
    
            total_network_out = torch.cat((total_network_out, output.unsqueeze(0)), 0) 
            all_nodes_on = np.concatenate((all_nodes_on, nodes_on))
            all_comms_on = np.concatenate((all_comms_on, comms_on))
        
            #To use BCE we apply sigmoid to outputs and do not normalize the targets
            targets = normalize(targets)
            
            test_predictions = np.append(test_predictions, output.data)
            test_targets = np.append(test_targets, targets.data)
            
           
            energy_total = len(nodes_on)
            
            energy_totals = np.append(energy_totals, energy_total)
            
            loss_1 = accuracy_criterion_1(output, targets)
            
            comm_target = 0.75 * energy_total 
    
            loss_2 = CommLoss(comms_count, comm_target)
            
            loss_3 = energy_loss(energy_total, grid_size)
            
            acc_impact = 1.0
            energy_impact = 0.5
            comm_impact = 1.0
            
            loss = (acc_impact * loss_1) + (comm_impact * loss_2) + (energy_impact * loss_3)
        
            running_loss += loss.data
            running_comm_loss += loss_2
            total_comms += comms_count

            total_node_count += all_node_count
            
            if k % 100 == 99:    # print every 1000 mini-batches
                print('[%5d] Accuracy loss: %.3f Communication loss: %.3f' %
              (k + 1, running_loss / 100, running_comm_loss / 100))
        
                losses = running_loss/100
                step_losses = np.append(step_losses, losses)
                comm_losses = running_comm_loss / 100
                step_comm_losses = np.append(step_comm_losses, comm_losses) 
                total_comms_list = np.append(total_comms_list, total_comms)
                total_comms = 0.0
                running_comm_loss = 0.0
                running_loss = 0.0
                #print('Energy Totals: ', energy_totals)
                sum_energy_totals = np.append(sum_energy_totals, sum(energy_totals))
                #print('Sum of Energy Totals: ', sum(energy_totals))
              
                train_mses = sqrt(mean_squared_error(test_targets, test_predictions))
                train_errors = error_rate(test_targets, test_predictions)
                step_mse = np.append(step_mse,train_mses)
                step_error = np.append(step_error,train_errors)
                
                _1 = np.sum(all_nodes_on == 0.0)
                _2 = np.sum(all_nodes_on == 1.0)
                _3 = np.sum(all_nodes_on == 2.0)
                _4 = np.sum(all_nodes_on == 3.0)
                _5 = np.sum(all_nodes_on == 4.0)
                _6 = np.sum(all_nodes_on == 5.0)
                _7 = np.sum(all_nodes_on == 6.0)
                _8 = np.sum(all_nodes_on == 7.0)
                _9 = np.sum(all_nodes_on == 8.0)
                _10 = np.sum(all_nodes_on == 9.0)
                _11 = np.sum(all_nodes_on == 10.0)
                _12 = np.sum(all_nodes_on == 11.0)
                _13 = np.sum(all_nodes_on == 12.0)
                _14 = np.sum(all_nodes_on == 13.0)
                _15 = np.sum(all_nodes_on == 14.0)
                _16 = np.sum(all_nodes_on == 15.0)
        
                nodes_on_all = np.asarray([_1, _2, _3, _4, _5, _6, _7,_8,_9,_10,_11, _12, _13, _14, _15, _16])
                nodes_on_all_todata = nodes_on_all.tolist()
            
            all_losses = np.append(all_losses, loss.data)
            
    mse = mean_squared_error(test_targets, test_predictions)
    rmse = sqrt(mse)
  
    error = error_rate(test_predictions, test_targets)
    
    all_comms_on_data = all_comms_on.tolist()
       
    return step_losses, mse, rmse, all_losses, test_predictions, test_targets, error, pred_write, step_mse, step_error, total_node_count, nodes_on_all_todata, all_comms_on_data
