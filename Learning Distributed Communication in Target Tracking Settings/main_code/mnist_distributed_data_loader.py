# -*- coding: utf-8 -*-
"""
Created on Sun Jul 21 14:52:41 2019

@author: priudu
"""


import torch
import torchvision

batch_size_train = 1000
batch_size_test = 1000


def load_mnist_data():
    train_loader = torch.utils.data.DataLoader(
      torchvision.datasets.MNIST('../mnist-data', train=True, download=True,
                                 transform=torchvision.transforms.Compose([
                                   torchvision.transforms.ToTensor(),
                                   torchvision.transforms.Normalize(
                                     (0.1307,), (0.3081,))
                                 ])),
      batch_size=batch_size_train, shuffle=True)
                                   
    test_loader = torch.utils.data.DataLoader(
      torchvision.datasets.MNIST('../mnist-data', train=False, download=True,
                                 transform=torchvision.transforms.Compose([
                                   torchvision.transforms.ToTensor(),
                                   torchvision.transforms.Normalize(
                                     (0.1307,), (0.3081,))
                                 ])),
      batch_size=batch_size_test, shuffle=True)
                                   
    return train_loader, test_loader

