# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 04:58:51 2020

@author: destinatoag
"""

from mnist_distributed_data_loader import load_mnist_data
import numpy as np  

mnist_train_data, mnist_test_data = load_mnist_data()     
    
def mnist_digits():   
    
        for (batch_idx, (data, target)) in enumerate(mnist_train_data):
            
           digit1 = np.asarray(np.where(target == 1))
           digit2 = np.asarray(np.where(target == 2))
           digit3 = np.asarray(np.where(target == 3))
           digit4 = np.asarray(np.where(target == 4))
           digit5 = np.asarray(np.where(target == 5))
           digit6 = np.asarray(np.where(target == 6))
           digit7 = np.asarray(np.where(target == 7))
           digit8 = np.asarray(np.where(target == 8))
           digit9 = np.asarray(np.where(target == 9))
             
        digit1_index = digit1[0][0]
        digit2_index = digit2[0][0]
        digit3_index = digit3[0][0]
        digit4_index = digit4[0][0]
        digit5_index = digit5[0][0]
        digit6_index = digit6[0][0]
        digit7_index = digit7[0][0]
        digit8_index = digit8[0][0]
        digit9_index = digit9[0][0]
       
        digit1 = data[digit1_index]
        target1 = target[digit1_index]
       
        digit2 = data[digit2_index]
        target2 = target[digit2_index]
       
        digit3 = data[digit3_index]
        target3 = target[digit3_index]
       
        digit4 = data[digit4_index]
        target4 = target[digit4_index]
       
        digit5 = data[digit5_index]
        target5 = target[digit5_index]
       
        digit6 = data[digit6_index]
        target6 = target[digit6_index]
       
        digit7 = data[digit7_index]
        target7 = target[digit7_index]
       
        digit8 = data[digit8_index]
        target8 = target[digit8_index]
       
        digit9 = data[digit9_index]
        target9 = target[digit9_index]
   
        return digit1, target1, digit2, target2, digit3, target3, digit4, target4, digit5, target5, digit6, target6, digit7, target7, digit8, target8, digit9, target9
    


