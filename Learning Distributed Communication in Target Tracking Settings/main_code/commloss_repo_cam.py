# -*- coding: utf-8 -*-
"""
Created on Thu May 16 01:26:13 2019

@author: destinatoag
"""


import numpy as np
import torch

def CommLoss(comm_count, target_count):
    target = target_count * 0.1
    comm = comm_count * 0.1
    
    loss = np.square(comm - target).mean(axis=None) 
    
    return loss 

def comm_decider(comm):
 
    if comm > -0.5:
        return torch.FloatTensor([comm])
    else:
        return torch.FloatTensor([0.0])
    


   
    



