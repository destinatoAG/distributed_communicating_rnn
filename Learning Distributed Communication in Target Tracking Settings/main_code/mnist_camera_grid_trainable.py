# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 15:02:45 2020

@author: destinatoag
"""

# -*- coding: utf-8 -*-
"""
Created on Tue May 26 15:35:04 2020

@author: destinatoag
"""

#16% faster that previous model 

import warnings
warnings.filterwarnings("ignore", category=FutureWarning)

import torch
from error_rate import error_rate
import torch.optim as optim
import torch.nn as nn
import torch.nn.functional as F 
from evaluator import test 
import matplotlib.pyplot as plt 
from mnist_camera_grid_data_states import states_pool, grid_size, tracking_steps, samples, hidden, sensor_in, target_size
from mnist_camera_grid_comms_handler_comm_loss import comm_states, comm_size
from commloss_repo_cam import CommLoss 
from datetime import datetime
import numpy as np
from sklearn.metrics import mean_squared_error
from math import sqrt

state_size = sensor_in + comm_size * 3 + hidden + target_size
 
cam_pos = 12
tar_pos = 13

print('Staging Data for...') 
print('Number of Network Nodes: ', grid_size)
print('Number of tracking steps: ', tracking_steps) 
print('Number of data samples to model: ', samples)
print('Overall Node State size: ', state_size)
print('Communication channel size: ', comm_size)

data_start=datetime.now() 
from mnist_camera_grid_data_states import Node 

all_data = Node.node_data(Node())
 
grid_of_grids = all_data[16]

print('Grid of grids shape from trainable shape: ', grid_of_grids.shape)

node1_inputs = all_data[0]
node2_inputs = all_data[1]
node3_inputs = all_data[2]
node4_inputs = all_data[3]
node5_inputs = all_data[4]
node6_inputs = all_data[5]
node7_inputs = all_data[6]
node8_inputs = all_data[7]
node9_inputs = all_data[8]
node10_inputs = all_data[9]
node11_inputs = all_data[10]
node12_inputs = all_data[11]
node13_inputs = all_data[12]
node14_inputs = all_data[13]
node15_inputs = all_data[14]
node16_inputs = all_data[15]

#For scalar targets 
all_node_data = all_data[17]

node1_inputs_scalar = all_node_data[0]
node2_inputs_scalar = all_node_data[1]
node3_inputs_scalar = all_node_data[2]
node4_inputs_scalar = all_node_data[3]
node5_inputs_scalar = all_node_data[4]
node6_inputs_scalar = all_node_data[5]
node7_inputs_scalar = all_node_data[6]
node8_inputs_scalar = all_node_data[7]
node9_inputs_scalar = all_node_data[8]
node10_inputs_scalar = all_node_data[9]
node11_inputs_scalar = all_node_data[10]
node12_inputs_scalar = all_node_data[11]
node13_inputs_scalar = all_node_data[12]
node14_inputs_scalar = all_node_data[13]
node15_inputs_scalar = all_node_data[14]
node16_inputs_scalar = all_node_data[15]

node_time = datetime.now()-data_start

print()
print('Finished Staging Node Train Data!')
print ("Time taken to stage node train data: %s" %(node_time))

#Timesteps must be same as tracking steps 
class Node(nn.Module):

    def __init__(self):
        super(Node, self).__init__()
        # These are the weights (common) in the whole network   
        #changed hidden from 50 to 10 
        self.fc1 = nn.Linear(state_size, 50)
        self.fc2 = nn.Linear(50, 8)
        self.fc3 = nn.Linear(8, state_size)
        
    def step(self,state):
        """ This is an internal function which runs a single step/iteration of a node"""
        "tied weights here"
        
        state = F.relu(F.linear(state,self.fc1.weight,self.fc1.bias))
        state =  F.relu(F.linear(state,self.fc2.weight,self.fc2.bias))
        state =  F.linear(state,self.fc3.weight,self.fc3.bias)
        return state
    
    def forward(self, input_data, input_data_scalar, py_states):
        """ This takes in an input sequence and uses it to run through the network"""
        
        comms_total = 0 
        nodes_on = torch.tensor([])
        comms_on = torch.tensor([])
        
        node_count_total = np.asarray([0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
       
        for k in range(grid_size):
            
            #looking at node sensors set to on
            if py_states[k][cam_pos] == 1.0: 
                #d gives the node position
                d = torch.FloatTensor([k])
                nodes_on = torch.cat((nodes_on, d), 0)
            
            #data input        
            py_states[k][0:sensor_in] = input_data[k]
            
            #Comparing original digits not features to expected tracked digit target 
            if input_data_scalar[k] == 1.0:
                py_states[k][tar_pos] = 1.0
            else:
                py_states[k][tar_pos] = 0.0
     
        #communication 
        py_states, comms_count, node_count = comm_states(py_states)
            
        #step update 
        for k in range(grid_size):
            
            if node_count[k] == 1.0:
                e = torch.FloatTensor([k])
                comms_on = torch.cat((comms_on, e), 0)
         
            py_states[k] = self.step(py_states[k].clone()) 
             
        all_cams = torch.tensor([])
        for k in range(grid_size):
            all_cams = torch.cat((all_cams, py_states[k][cam_pos].view(1)), 0) 
        
        maxcam = torch.max(all_cams) 
        mincam = torch.min(all_cams)
     
        cut_factor = (maxcam - mincam) / 2
      
        #else to 0 when optimisation to 1 when there is non
        for k in range(grid_size):
            if all_cams[k] < cut_factor: 
                py_states[k][cam_pos] = torch.FloatTensor([1.0])
            else:
                py_states[k][cam_pos] = torch.FloatTensor([0.0])
        
        comms_total += comms_count
        node_count_total += node_count
        
        all_nodes_out = torch.tensor([])
          
        for k in range(grid_size):
            all_nodes_out = torch.cat((all_nodes_out, py_states[k][-1].unsqueeze(0)))
                
        return all_nodes_out, comms_total, node_count_total, nodes_on, comms_on  
        
net = Node()
net.zero_grad()

accuracy_criterion_1 = nn.MSELoss()
accuracy_criterion_2 = nn.BCELoss()
accuracy_criterion_3 = nn.BCEWithLogitsLoss()
m = nn.Sigmoid()

lr = 0.0005

optimizer = optim.Adam(net.parameters(), lr)

params = list(net.parameters())

#Data attributes 
def count_parameters(net):
    return sum(p.numel() for p in net.parameters() if p.requires_grad)

params_trainable = count_parameters(net)
num_neurons = 8
params_all = sum(p.numel() for p in net.parameters())
params = list(net.parameters())
layers_total = len(params)
hidden_total = layers_total - 2

print()
print('Total Number of Layers: ', layers_total)
print('Hidden Layers: ', hidden_total)
print('Parameters Number - Trainable: ', params_trainable)
print('Parameters Number - ALL: ', params_all)
print('Learning rate: ', lr)
print('Number of training samples: ', samples)

input_len = (tracking_steps*samples) 

# Now for the training
train_start=datetime.now() 

predictions = np.asarray([])
energy_totals = np.asarray([])

step_losses = np.asarray([])
targets_ = np.asarray([])

running_loss = 0.0
running_comm_loss = 0.0
losses = 0
total_comms = 0.0

t_new = np.asarray([])
all_losses = np.asarray([])

all_nodes_on = np.asarray([])
all_comms_on = np.asarray([])

all_comm_losses = np.asarray([])
total_comms_list = np.asarray([])
comm_targets = np.asarray([])
step_losses = np.asarray([])
step_comm_losses = np.asarray([])

step_rmse = np.asarray([])
sum_energy_totals = np.asarray([])
sum_comm_totals = np.asarray([])
sum_comm_totals_list = np.asarray([])

step_energies = np.asarray([])
total_node_count = np.asarray([0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
all_node_counts = np.asarray([0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0])
        

step_error = np.asarray([])
total_network_out = torch.tensor([])

 
def energy_loss(energy_total, grid_size):
    loss = (energy_total / grid_size)  
    return loss 

def normalize(data):
    maxt = torch.max(data)
    x_normed = data / maxt
    return x_normed 

threshold = 0.5   


for k in range(input_len): 
    
    if k == input_len - 1: 
         break
     
    input_data = torch.tensor([])
    targets = torch.tensor([])
    
    input_data = torch.cat((input_data, node1_inputs[k].view(1,sensor_in)), 0) 
    input_data = torch.cat((input_data, node2_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node3_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node4_inputs[k].view(1,sensor_in)), 0) 
    input_data = torch.cat((input_data, node5_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node6_inputs[k].view(1,sensor_in)), 0) 
    input_data = torch.cat((input_data, node7_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node8_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node9_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node10_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node11_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node12_inputs[k].view(1,sensor_in)), 0) 
    input_data = torch.cat((input_data, node13_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node14_inputs[k].view(1,sensor_in)), 0) 
    input_data = torch.cat((input_data, node15_inputs[k].view(1,sensor_in)), 0)
    input_data = torch.cat((input_data, node16_inputs[k].view(1,sensor_in)), 0)
    
    input_data_scalar = [node1_inputs_scalar[k].view(1), node2_inputs_scalar[k].view(1), node3_inputs_scalar[k].view(1), node4_inputs_scalar[k].view(1), node5_inputs_scalar[k].view(1), node6_inputs_scalar[k].view(1), node7_inputs_scalar[k].view(1), node8_inputs_scalar[k].view(1), node9_inputs_scalar[k].view(1), node10_inputs_scalar[k].view(1), node11_inputs_scalar[k].view(1), node12_inputs_scalar[k].view(1), node13_inputs_scalar[k].view(1), node14_inputs_scalar[k].view(1), node15_inputs_scalar[k].view(1), node16_inputs_scalar[k].view(1)]
    
    input_data_scalar = torch.stack(input_data_scalar)
    
    targets = [node1_inputs_scalar[k+1].view(1), node2_inputs_scalar[k+1].view(1), node3_inputs_scalar[k+1].view(1), node4_inputs_scalar[k+1].view(1), node5_inputs_scalar[k+1].view(1), node6_inputs_scalar[k+1].view(1), node7_inputs_scalar[k+1].view(1), node8_inputs_scalar[k+1].view(1), node9_inputs_scalar[k+1].view(1), node10_inputs_scalar[k+1].view(1), node11_inputs_scalar[k+1].view(1), node12_inputs_scalar[k+1].view(1), node13_inputs_scalar[k+1].view(1), node14_inputs_scalar[k+1].view(1), node15_inputs_scalar[k+1].view(1), node16_inputs_scalar[k+1].view(1)]
    
    targets = torch.stack(targets)
    
    if k % tracking_steps == 0.0: 
          py_states = states_pool(grid_size, state_size)
       
    targets = targets.view(-1)
   
    output, comms_count, all_node_count, nodes_on, comms_on = net(input_data, input_data_scalar, py_states)
    
    nodes_on = nodes_on.detach().numpy()
    comms_on = comms_on.detach().numpy()
    
    total_network_out = torch.cat((total_network_out, output.unsqueeze(0)), 0) 
    
    all_nodes_on = np.concatenate((all_nodes_on, nodes_on))
    all_comms_on = np.concatenate((all_comms_on, comms_on))
    
    #To use BCE we apply sigmoid to outputs and do not normalize the targets
    targets = normalize(targets)
 
    optimizer.zero_grad()   
    
    predictions = np.append(predictions, output.data)
    targets_ = np.append(targets_, targets.data)
  
    energy_total = len(nodes_on)
    
    energy_totals = np.append(energy_totals, energy_total)
    sum_comm_totals = np.append(sum_comm_totals, comms_count)
    
    loss_1 = accuracy_criterion_1(output, targets)
    
    #Depends on optimisation (medium, high, low)
    comm_target = 0.75 * energy_total 
    
    loss_2 = CommLoss(comms_count, comm_target)

    loss_3 = energy_loss(energy_total, grid_size)
    
    acc_impact = 1.0
    energy_impact = 0.5
    comm_impact = 1.0
    
    loss = (acc_impact * loss_1) + (comm_impact * loss_2) + (energy_impact * loss_3)

    loss.backward(retain_graph=True) 
    
    optimizer.step()        
    
    running_loss += loss.data
    running_comm_loss += loss_2
    total_comms += comms_count
    total_node_count += all_node_count
    
    if k % 100 == 99:    # print every 100 mini-batches
        print('[%5d] Accuracy loss: %.3f Communication loss: %.3f' %
              (k + 1, running_loss / 100, running_comm_loss / 100))
        
        losses = running_loss/ 100
        step_losses = np.append(step_losses, losses)
        comm_losses = running_comm_loss / 100
        step_comm_losses = np.append(step_comm_losses, comm_losses) 
        running_loss = 0.0
        total_comms_list = np.append(total_comms_list, total_comms)
        total_comms = 0.0
        running_comm_loss = 0.0
        print('Energy Totals: ', energy_totals)
        sum_energy_totals = np.append(sum_energy_totals, sum(energy_totals))
        print('Sum of Energy Totals: ', sum(energy_totals))
     
        train_rmses = sqrt(mean_squared_error(targets_, predictions))
        train_errors = error_rate(targets_, predictions)
        step_rmse = np.append(step_rmse,train_rmses)
        step_error = np.append(step_error,train_errors)
        
        _1 = np.sum(all_nodes_on == 0.0)
        _2 = np.sum(all_nodes_on == 1.0)
        _3 = np.sum(all_nodes_on == 2.0)
        _4 = np.sum(all_nodes_on == 3.0)
        _5 = np.sum(all_nodes_on == 4.0)
        _6 = np.sum(all_nodes_on == 5.0)
        _7 = np.sum(all_nodes_on == 6.0)
        _8 = np.sum(all_nodes_on == 7.0)
        _9 = np.sum(all_nodes_on == 8.0)
        _10 = np.sum(all_nodes_on == 9.0)
        _11 = np.sum(all_nodes_on == 10.0)
        _12 = np.sum(all_nodes_on == 11.0)
        _13 = np.sum(all_nodes_on == 12.0)
        _14 = np.sum(all_nodes_on == 13.0)
        _15 = np.sum(all_nodes_on == 14.0)
        _16 = np.sum(all_nodes_on == 15.0)

        nodes_on_all = np.asarray([_1, _2, _3, _4, _5, _6, _7,_8,_9,_10,_11, _12, _13, _14, _15, _16])
        nodes_on_all_todata = nodes_on_all.tolist()
        sum_comm_totals_list = np.append(sum_comm_totals_list, sum(sum_comm_totals))
        
        #print('Nodes on: ', nodes_on_all_todata)
        #print('Total node comms: ', total_node_count)
        #print('Sum of Comm Totals: ', sum(sum_comm_totals))
   
    all_losses = np.append(all_losses, loss.data)
    all_comm_losses = np.append(all_comm_losses, loss_2)
    all_node_counts += all_node_count


#torch.save(net.state_dict(), '../models/mnist_8steps_opt_16n_model_test.pth')
#torch.save(optimizer.state_dict(), '../models/mnist_8steps_opt_16n_optimiser_test.pth')

train_mse = mean_squared_error(targets_, predictions)
train_rmse = sqrt(train_mse)

print('Total network output: ', total_network_out.shape)
print('Energy totals: ', energy_totals.shape)
print('All nodes on: ', all_nodes_on.shape)
print('All nodes totals len: ', len(all_nodes_on))

energy_total_ = len(all_nodes_on)

energy_totals_todata = energy_totals.tolist()
all_nodes_on_todata = all_nodes_on.tolist()
all_comms_on_todata = all_comms_on.tolist()

print('Train RMSE: ', train_rmse)
print('Step MSE: ', step_rmse)
print('Step Error: ', step_error) 


train_time = datetime.now()-train_start
print ("Time to Train Network %s" %(train_time))

fig_13, ax6 = plt.subplots(figsize=(5,3))
plt.title('Training Losses')
plt.legend(['Train Loss'], loc='upper right')
plt.plot(step_losses, color='blue')
plt.show(ax6)

fig_14, ax7 = plt.subplots(figsize=(5,3))
plt.title('Communication Losses')
plt.legend(['Comm Loss'], loc='upper right')
plt.plot(step_comm_losses, color='orange')
plt.show(ax7)

_1 = np.sum(all_nodes_on == 0.0)
_2 = np.sum(all_nodes_on == 1.0)
_3 = np.sum(all_nodes_on == 2.0)
_4 = np.sum(all_nodes_on == 3.0)
_5 = np.sum(all_nodes_on == 4.0)
_6 = np.sum(all_nodes_on == 5.0)
_7 = np.sum(all_nodes_on == 6.0)
_8 = np.sum(all_nodes_on == 7.0)
_9 = np.sum(all_nodes_on == 8.0)
_10 = np.sum(all_nodes_on == 9.0)
_11 = np.sum(all_nodes_on == 10.0)
_12 = np.sum(all_nodes_on == 11.0)
_13 = np.sum(all_nodes_on == 12.0)
_14 = np.sum(all_nodes_on == 13.0)
_15 = np.sum(all_nodes_on == 14.0)
_16 = np.sum(all_nodes_on == 15.0)

nodes_on_all = np.asarray([_1, _2, _3, _4, _5, _6, _7,_8,_9,_10,_11, _12, _13, _14, _15, _16])
nodes_on_all_todata = nodes_on_all.tolist()

print('Staging Test Data...') 
test_start=datetime.now() 
from mnist_camera_grid_data_states import Node 

all_test_data = Node.test_data(Node())

test_grid_of_grids = all_test_data[16]

test_time = datetime.now()-test_start

print()
print('Finished Staging Test Data!')
print ("Time taken to stage test data: %s" %(test_time))

test_len = tracking_steps * 75

py_states = states_pool(grid_size, state_size)

node_input_scalar = [node1_inputs_scalar, node2_inputs_scalar, node3_inputs_scalar, node4_inputs_scalar, node5_inputs_scalar, node6_inputs_scalar, node7_inputs_scalar, node8_inputs_scalar, node9_inputs_scalar, node10_inputs_scalar, node11_inputs_scalar, node12_inputs_scalar, node13_inputs_scalar, node14_inputs_scalar, node15_inputs_scalar, node16_inputs_scalar]

x = test(net, all_test_data, test_len, states_pool, py_states, input_len, sensor_in, node_input_scalar, tracking_steps, grid_size, state_size)

test_losses = x[0]
mse = x[1]
rmse = x[2]
all_test_losses = x[3]
test_predictions = x[4]
test_target = x[5]
error = x[6]
pred_write = x[7]
test_step_mse = x[8]
test_step_error = x[9]
test_node_count = x[10]
test_nodes_on = x[11]
test_comms_on = x[12]

print('Task: Max')
print('Results on test set!')
print('MSE achieved: ', mse)  
print('RMSE achieved: ', rmse)
print('Error Rate: ', error)

file = open('results.txt', 'w')

file.writelines('Number of Network Nodes: ' + repr(grid_size) + '\n')
file.writelines('Overall Node State size: ' + repr(state_size) + '\n')
file.writelines('Communication channel size: ' + repr(comm_size) + '\n')
file.writelines('Number of Tracking steps: ' + repr(tracking_steps) + '\n')
file.writelines('Total Number of Layers: ' + repr(layers_total) + '\n')
file.writelines('Hidden Layers: ' + repr(hidden_total) + '\n') 
file.writelines('Parameters Number - Trainable: ' + repr(params_trainable) + '\n')
file.writelines('Parameters Number - ALL: ' + repr(params_all) + '\n')
file.writelines('Number of training samples: ' + repr(samples) + '\n') 
file.writelines('Training Losses: ' + repr(step_losses) + '\n') 
file.writelines('Communication Losses: ' + repr(step_comm_losses) + '\n') 
file.writelines('Time taken to stage node train data: ' + repr(node_time) + '\n') 
file.writelines('Time taken to train network: ' + repr(train_time) + '\n') 
file.writelines('Lowest Train Loss, at ' + repr(k+1) + ' steps/epochs: ' + repr(step_losses[-1]) + '\n') 
file.writelines('Train MSE achieved: ' + repr(train_mse) + '\n') 
file.writelines('Train RMSE achieved: ' + repr(train_rmse) + '\n') 
file.writelines('Original Predictions: ' + repr(total_network_out) + '\n') 
file.writelines('Normalised targets: ' + repr(targets_) + '\n') 
file.writelines('Step RMSE: ' + repr(step_rmse) + '\n') 
file.writelines('Step Error: ' + repr(step_error) + '\n') 
file.writelines('Final Energy Used: ' + repr(energy_total_) + '\n')
file.writelines('All Nodes On Quantities: ' + repr(nodes_on_all_todata) + '\n')
file.writelines('All node Communication totals: ' + repr(total_node_count) + '\n') 
file.writelines('Sum of Energy Totals: ' + repr(sum_energy_totals) + '\n') 
file.writelines('Sum of Comm Totals: ' + repr(sum_comm_totals_list) + '\n') 
file.writelines('Test MSE achieved: ' + repr(mse) + '\n') 
file.writelines('Test RMSE achieved: ' + repr(rmse) + '\n')
file.writelines('Test Error achieved: ' + repr(error) + '\n')
file.writelines('Test Losses achieved: ' + repr(test_losses) + '\n')
file.writelines('Step Test MSE achieved: ' + repr(test_step_mse) + '\n')
file.writelines('Step Test Error achieved: ' + repr(test_step_error) + '\n')
file.writelines('Energy Totals: ' + repr(energy_totals[0:20]) + '\n')
file.writelines('Energy Totals 1: ' + repr(energy_totals[20:50]) + '\n')
file.writelines('Energy Totals 2: ' + repr(energy_totals[50:80]) + '\n')
file.writelines('All nodes on : ' + repr(all_nodes_on[0:100]) + '\n')
file.writelines('All nodes on 1: ' + repr(all_nodes_on[100:200]) + '\n')
file.writelines('All nodes on 2: ' + repr(all_nodes_on[200:300]) + '\n')
file.writelines('All comms on : ' + repr(all_comms_on[0:100]) + '\n')
file.writelines('All comms on 1: ' + repr(all_comms_on[100:200]) + '\n')
file.writelines('All comms on 2: ' + repr(all_comms_on[200:300]) + '\n')
file.writelines('List of grids: ' + repr(grid_of_grids[0:20]) + '\n')
file.writelines('List of grids 1: ' + repr(grid_of_grids[20:50]) + '\n')
file.writelines('List of grids 2: ' + repr(grid_of_grids[50:80]) + '\n')
file.writelines('Test List of grids: ' + repr(test_grid_of_grids[0:20]) + '\n')
file.writelines('Test List of grids 1: ' + repr(test_grid_of_grids[20:50]) + '\n')
file.writelines('Test List of grids 2: ' + repr(test_grid_of_grids[50:80]) + '\n')
file.writelines('Test all Nodes On: ' + repr(test_nodes_on) + '\n')
file.writelines('Test Comm Nodes Total: ' + repr(test_node_count) + '\n')
file.writelines('All Energy Totals: ' + repr(energy_totals_todata) + '\n')
file.writelines('All Nodes On: ' + repr(all_nodes_on_todata) + '\n')
file.writelines('All Comms On: ' + repr(all_comms_on_todata) + '\n')
file.close()
