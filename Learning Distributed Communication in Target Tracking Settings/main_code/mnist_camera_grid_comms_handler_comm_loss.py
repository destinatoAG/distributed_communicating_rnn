# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 14:44:10 2020

@author: destinatoag
"""

# -*- coding: utf-8 -*-
"""
Created on Wed Aug 28 15:40:51 2019

@author: priudu
"""

import torch
import numpy as np  
from commloss_repo_cam import comm_decider  
#from node_16_naive_number_tracker_data_states import grid_size, net_matrix_torch, sensor_in, hidden, target_size 
from mnist_camera_grid_data_states import grid_size, net_matrix_torch, sensor_in, hidden, target_size

comm_size = 1
node_connections = 2 

input_comms_start_position = sensor_in
input_comms_end_position = (node_connections*comm_size) - 1 + sensor_in 
output_comms_start_position = sensor_in + (node_connections*comm_size) - 1 
output_comms_end_position = sensor_in + (3*comm_size) - 1 

state_size = sensor_in + comm_size * 3 + hidden + target_size

cam_pos = 12

def comm_states(new_states):
    
    comms_count = 0.0 
    count = 0
    
    node_count = np.asarray([])
    
    comm_out = torch.tensor([])
    
    for i in range(grid_size): 
        
        #Zeroing inputs and comms based on cam state 
        if new_states[i][cam_pos] == 0.0:
            new_states[i][output_comms_start_position:output_comms_end_position] = 0.0
            new_states[i][0] = 0.0 
            
        comm = new_states[i][output_comms_start_position:output_comms_end_position]
   
        comm_out = torch.cat((comm_out,comm))
         
    #Lossy links
    #comm_out[0] = 0.0
    #comm_out[4] = 0.0 

    comms_out = net_matrix_torch * comm_out
    
    for k in range(grid_size):
        
        node_comm = comms_out[k][comms_out[k].nonzero()].view(-1)  
        
        #Checking to see if node being evaluated has 0 inputs 
        if len(node_comm) == 0:
            node_comm = torch.Tensor([0.0])
        
        node_comm, indices = torch.max(node_comm, 0)
        
        node_comm = comm_decider(node_comm)
        
        zeros = torch.zeros(1)
        
        #Resolving dynamic comm size inputs
        for a in range(comm_size*1):
            if node_comm.shape[0] != comm_size*1: 
                node_comm = torch.cat((node_comm, zeros))
        
        new_states[k][input_comms_start_position:input_comms_end_position] = node_comm
        
        #Setting input channels to 0.0 for non-comm nodes
        if new_states[k][cam_pos] == 0.0:
            new_states[k][input_comms_start_position:input_comms_end_position] = 0.0
        
        #Counting only communicating/non-off nodes 
        if new_states[k][input_comms_start_position:input_comms_end_position] != 0.0:
            count = 1
            node_count = np.append(node_count,count)
            comms_count += 1
        else:
            count = 0 
            node_count = np.append(node_count,count)
            
    return new_states, comms_count, node_count 









