#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 14:46:14 2020

@author: destinatoag
"""

import pandas as pd 
import numpy as np 
import matplotlib.pyplot as plt 


total_arr = np.asarray([1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200])

#High energy optimisation case with 27.4 % energy usage
high_actual = np.asarray([406, 346, 330, 325, 316, 321, 335, 317, 331, 317, 331, 315, 315, 323, 315, 330])

#Medium energy optimisation case with 54.5% usage 
medium_actual = np.asarray([772, 719, 706, 677, 742, 710, 709, 690, 708, 700, 701, 699, 715, 696, 707, 677])

#Low energy optimisation case with 71.7% usage 
low_actual = np.asarray([919, 876, 851, 839, 889, 903, 833, 816, 883, 850, 862, 856, 869, 838, 855, 842])

diff_high = total_arr - high_actual 
diff_medium = total_arr - medium_actual 
diff_low = total_arr - low_actual 

#epochs = np.arange(5)
elabels = ['1','2','3','4','5', '6','7','8','9','10', '11','12','13','14','15', '16']

index = ['Node 1', 'Node 2', 'Node 3',
          'Node 4', 'Node 5', 'Node 6', 'Node 7', 'Node 8',
          'Node 9', 'Node 10', 'Node 11', 'Node 12', 'Node 13',
          'Node 14', 'Node 15', 'Node 16']

df_high = pd.DataFrame({'Node Comm': high_actual, 
                    'Difference': diff_high} , index=index)


df_medium = pd.DataFrame({'Node Comm': medium_actual, 
                    'Difference': diff_medium} , index=index)

df_low = pd.DataFrame({'Node Comm': low_actual, 
                    'Difference': diff_low} , index=index)


fig = df_high.plot.barh(stacked='True', figsize=(8,6), fontsize=12).legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

fig = df_medium.plot.barh(stacked='True', figsize=(8,6), fontsize=12).legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

fig = df_low.plot.barh(stacked='True', figsize=(8,6), fontsize=12).legend(bbox_to_anchor=(1.0, 1.0)).get_figure()


#-----------------------------------------------------------------------------------
print('Plotting Energies!')

total_arr = np.asarray([1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200])

#High energy optimisation case with 36.6 % energy usage
high_actual = np.asarray( [512, 461, 442, 414, 434, 443, 437, 440, 425, 444, 455, 429, 427, 451, 410, 422])

#Medium energy optimisation case with 59.1% usage 
medium_actual = np.asarray([792, 746, 699, 697, 755, 738, 702, 701, 704, 687, 687, 676, 698, 687, 698, 693])

#Low energy optimisation case with 72.3% usage 
low_actual = np.asarray([934, 880, 873, 841, 872, 865, 852, 867, 870, 895, 855, 849, 853, 886, 851, 848])

diff_high = total_arr - high_actual 
diff_medium = total_arr - medium_actual 
diff_low = total_arr - low_actual 

#epochs = np.arange(5)
elabels = ['1','2','3','4','5', '6','7','8','9','10', '11','12','13','14','15', '16']

index = ['Node 1', 'Node 2', 'Node 3',
          'Node 4', 'Node 5', 'Node 6', 'Node 7', 'Node 8',
          'Node 9', 'Node 10', 'Node 11', 'Node 12', 'Node 13',
          'Node 14', 'Node 15', 'Node 16']

df_high = pd.DataFrame({'Node Comm': high_actual, 
                    'Difference': diff_high} , index=index)


df_medium = pd.DataFrame({'Node Comm': medium_actual, 
                    'Difference': diff_medium} , index=index)

df_low = pd.DataFrame({'Node Comm': low_actual, 
                    'Difference': diff_low} , index=index)


fig = df_high.plot.barh(stacked='True', figsize=(8,6), fontsize=12, colormap='summer').legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

fig = df_medium.plot.barh(stacked='True', figsize=(8,6), fontsize=12, colormap='summer').legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

fig = df_low.plot.barh(stacked='True', figsize=(8,6), fontsize=12, colormap='summer').legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

print('Plotting Comms!')
#----------------------------------------------------------------------------------------
total_arr = np.asarray([1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200, 1200])

#High energy optimisation case with 36.6 % energy usage
high_actual = np.asarray([222., 233., 212., 222., 223., 232., 220., 261., 253., 228., 237.,
       220., 226., 223., 227., 227.])

#Medium energy optimisation, medium comms with 59.1% usage
medium_actual = np.asarray([419., 458., 461., 455., 484., 443., 460., 500., 508., 451., 404.,
       422., 428., 421., 412., 455.])

#Low energy optimisation, high comms case with 72.3% usage 
low_actual = np.asarray([663., 663., 640., 626., 645., 622., 637., 655., 686., 671., 644.,
       614., 647., 644., 635., 646.])

diff_high = total_arr - high_actual 
diff_medium = total_arr - medium_actual 
diff_low = total_arr - low_actual 

#epochs = np.arange(5)
elabels = ['1','2','3','4','5', '6','7','8','9','10', '11','12','13','14','15', '16']

index = ['Node 1', 'Node 2', 'Node 3',
          'Node 4', 'Node 5', 'Node 6', 'Node 7', 'Node 8',
          'Node 9', 'Node 10', 'Node 11', 'Node 12', 'Node 13',
          'Node 14', 'Node 15', 'Node 16']

df_high = pd.DataFrame({'Node Comm': high_actual, 
                    'Difference': diff_high} , index=index)


df_medium = pd.DataFrame({'Node Comm': medium_actual, 
                    'Difference': diff_medium} , index=index)

df_low = pd.DataFrame({'Node Comm': low_actual, 
                    'Difference': diff_low} , index=index)


fig = df_high.plot.barh(stacked='True', figsize=(8,6), fontsize=12, colormap='winter').legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

fig = df_medium.plot.barh(stacked='True', figsize=(8,6), fontsize=12, colormap='winter').legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

fig = df_low.plot.barh(stacked='True', figsize=(8,6), fontsize=12, colormap='winter').legend(bbox_to_anchor=(1.0, 1.0)).get_figure()

#----------------------------------------------------------------------------------------

#Step MSE, STEP error () 

step_rmse =  [0.49428709, 0.42557432, 0.39105642, 0.36269298, 0.34077654,
       0.32382574, 0.30935628, 0.29941191, 0.29120676, 0.28451465,
       0.27814215]

step_error =  [2.63044391, 1.74836015, 1.34305329, 1.09738502, 0.93617168,
       0.82254323, 0.73890801, 0.6775817 , 0.62895568, 0.59031121,
       0.55697283]


#72.3% energy usage, low optimisation 

step_rmse_low =  [0.72484625, 0.58432781, 0.52194425, 0.47862244, 0.45014005,
       0.4258388 , 0.40391222, 0.38548033, 0.36921153, 0.35643481,
       0.34478989]

step_error_low =  [1.64710995,  0.81439831,  0.96128588,  0.88377225,  0.81689815,
        0.75065066,  0.68497508,  0.62876986,  0.58309277,  0.54816698,
        0.51685385]

#Medium Optimisation 54.5% energy usage results 
messages_medium = [114.4, 99.2, 94.4, 90.5, 93.8, 93.5, 92.5, 93.7, 89.6, 95.9, 89.3]

#High Optimisation with 26.6% energy usage results 
messages_high =  [114.4, 99.2, 94.4, 90.5, 93.8, 93.5, 92.5, 93.7, 89.6, 95.9, 89.3]

messages_low = np.asarray([1392, 1124, 939, 1060, 967, 858, 694, 657, 674, 673, 636])

messages_low = np.round((messages_low / 12.5), decimals=0)

print(messages_low)

#Divide this by 12.5 to get comms per foward push as we do 1444/12.5, since in a 100 runs we have (100/8) = 12.5 pushes  

messages_low = np.sort(messages_low)

fig_1a, ax1a = plt.subplots(figsize=(12,8))
plt.title('Accuracy (8 Tracking steps)', fontsize=22)
ax1a.plot(messages_low, step_rmse_low, color = 'blue', label='RMSE', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.plot(messages_low,step_error_low, color='orange', label='Error % ', linestyle='dashed',  marker='o', markersize=6, linewidth=2)
ax1a.legend(loc='upper right', fontsize = 'xx-large')
#ax1a.set_xticks(np.arange(98,120,5))
plt.ylabel('Accuracy', fontsize=22)
plt.xlabel('# of messages communicated overall', fontsize=22)
plt.setp(ax1a.get_xticklabels(), fontsize=22)
plt.setp(ax1a.get_yticklabels(), fontsize=22)
plt.grid(True)
plt.show(ax1a)


'''
r100 =  np.asarray([100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100])
r200 =  np.asarray([159, 165, 174, 172, 164, 172, 179, 174, 178, 172, 185, 172, 172, 176, 172, 180])
r300 = np.asarray([178, 181, 194, 186, 180, 185, 196, 187, 192, 189, 201, 185, 189, 193, 186, 199])
r400 = np.asarray([204, 194, 212, 200, 198, 198, 213, 203, 208, 202, 213, 198, 202, 209, 200, 211])
r500 = np.asarray([231, 214, 225, 216, 213, 213, 231, 217, 225, 216, 226, 212, 216, 224, 213, 227])
r600 = np.asarray([255, 239, 241, 229, 227, 230, 244, 231, 238, 231, 241, 229, 229, 237, 226, 240])
r700 = np.asarray([281, 254, 256, 247, 241, 246, 258, 248, 256, 245, 258, 245, 243, 251, 243, 255])
r800 =  np.asarray([305, 272, 270, 260, 255, 261, 273, 260, 271, 258, 273, 258, 259, 265, 258, 269])
r900 = np.asarray([331, 287, 284, 277, 268, 274, 288, 275, 287, 274, 288, 271, 274, 280, 273, 285])
r1000 = np.asarray([355, 303, 298, 294, 280, 290, 301, 290, 301, 287, 302, 285, 286, 295, 287, 299])
r1100 = np.asarray([381, 327, 316, 310, 298, 306, 318, 303, 318, 304, 317, 302, 301, 310, 302, 315])
'''

#72.3 % energy usage, that is LOW optimisation

r100 =  np.asarray([87., 87., 87., 87., 87., 87., 87., 87., 87., 87., 87., 87., 87., 87., 87., 87.])
r200 =  np.asarray([163., 159., 167., 164., 154., 160., 164., 161., 154., 155., 160., 152., 149., 150.,
 157., 147.])
r300 = np.asarray( [221., 228., 226., 221., 209., 215., 223., 219., 221., 225., 221., 198., 202., 210.,
 210., 206.])
r400 = np.asarray([294., 291., 293., 295., 290., 280., 281., 274., 288., 294., 286., 263., 269., 271.,
 272., 274.])
r500 = np.asarray([359., 354., 348., 345., 344., 333., 333., 342., 346., 352., 345., 327., 340., 336.,
 339., 339.])
r600 = np.asarray([406., 402., 395., 396., 395., 384., 385., 393., 407., 416., 404., 373., 409., 387.,
 388., 400.])
r700 = np.asarray([450., 447., 441., 430., 440., 431., 429., 435., 458., 454., 445., 416., 455., 434.,
 433., 436.])
r800 =  np.asarray([486., 491., 474., 465., 486., 465., 470., 469., 501., 501., 489., 465., 495., 474.,
 475., 485.])
r900 = np.asarray([534., 534., 519., 513., 527., 502., 507., 512., 550., 540., 533., 513., 527., 514.,
 516., 524.])
r1000 = np.asarray([579., 574., 558., 551., 572., 545., 553., 566., 598., 584., 572., 554., 568., 554.,
 548., 562.])
r1100 = np.asarray([622., 612., 592., 593., 610., 585., 594., 603., 640., 630., 613., 579., 607., 600.,
 590., 604.])

r100av = np.average(r100)
r100std = np.std(r100)

r300 = r300 - r200
r300av = np.average(r300)
r300std = np.std(r300)

r500 = r500 - r400
r500av = np.average(r500)
r500std = np.std(r500)

r700 = r700 - r600
r700av = np.average(r700)
r700std = np.std(r700)

r900 = r900 - r800
r900av = np.average(r900)
r900std = np.std(r900)

r1100 = r1100 - r1000
r1100av = np.average(r1100)
r1100std = np.std(r1100)

epochs = np.arange(6)
elabels = ['1','2','3','4','5','6']
node_means = [r100av, r300av, r500av, r700av, r900av, r1100av]
node_error = [r100std, r300std, r500std, r700std, r900std, r1100std]


# Build the plot
fig, ax = plt.subplots(figsize=(10,8))
ax.bar(epochs, node_means, yerr=node_error, align='center', ecolor='black', capsize=10, color='royalblue')
plt.title('Avg. Comms (8 Tracking steps)', fontsize=22)
ax.set_ylabel('Avg. number of messages communicated', fontsize=22)
ax.set_xlabel('Epochs', fontsize=22)
ax.set_xticks(epochs)
ax.set_xticklabels(elabels)
plt.setp(ax.get_xticklabels(), fontsize=22)
plt.setp(ax.get_yticklabels(), fontsize=22)
ax.yaxis.grid(True)
plt.tight_layout()
plt.show()

#------------------

def plot_stacked_bar(data, series_labels, category_labels=None, 
                     show_values=False, value_format="{}", y_label=None, 
                     colors=None, grid=False, reverse=False):
    
    
    ny = len(data[0])
    ind = list(range(ny))

    axes = []
    cum_size = np.zeros(ny)

    data = np.array(data)

    if reverse:
        data = np.flip(data, axis=1)
        category_labels = reversed(category_labels)

    for i, row_data in enumerate(data):
        axes.append(plt.bar(ind, row_data, bottom=cum_size, 
                            label=series_labels[i], color=colors[i]))
        cum_size += row_data

    if category_labels:
        plt.xticks(ind, category_labels, fontsize='22')

    if y_label:
        plt.ylabel(y_label)

    plt.legend(fontsize='xx-large')

    if grid:
        plt.grid()

    if show_values:
        for axis in axes:
            for bar in axis:
                w, h = bar.get_width(), bar.get_height()
                plt.text(bar.get_x() + w/2, bar.get_y() + h/2, 
                         value_format.format(h), ha="center", 
                         va="center")
 
'''
fig, ax = plt.subplots(figsize=(11,8))

series_labels = ['BCE', 'SNT']

data = [
    [32100, 77040, 128400],
    [54225, 130140, 216900],
]

category_labels = ['8', '12', '16']


plot_stacked_bar(
    data, 
    series_labels, 
    category_labels=category_labels, 
    show_values=True, 
    value_format="{:.1f}",
    colors=['steelblue', 'goldenrod', 'tab:green' ],
    
)

#plt.title('', fontsize=26)
plt.setp(ax.get_xticklabels(), fontsize=22) 
plt.setp(ax.get_yticklabels(), fontsize=22) 

ticks = ax.get_yticks()*0.001
ticks = ticks.astype(int)
ax.set_yticklabels(ticks)
plt.title('Energy Usage', fontsize=22)
plt.xlabel('Tracking Steps', fontsize=26)
plt.ylabel('Energy Used (Nodes ON)', fontsize=26)
plt.savefig('../figures/bce_snt_computation.png', dpi=600)
plt.show(ax)
'''

fig, ax = plt.subplots(figsize=(11,8))

series_labels = ['Energy', 'Comms', 'Energy Totals']

data = [
    [19184, 29975], 
    [7046, 11022],
    [3666, 4000],
    
]

category_labels = ['16', '25']

plot_stacked_bar(
    data, 
    series_labels, 
    category_labels=category_labels, 
    show_values=True, 
    value_format="{:.1f}",
    colors=['steelblue', 'goldenrod', 'tab:green' ],
    
)

#plt.title('', fontsize=26)
plt.title('Optimisation', fontsize=22)
plt.setp(ax.get_xticklabels(), fontsize=22) 
plt.setp(ax.get_yticklabels(), fontsize=22) 

ticks = ax.get_yticks()*0.001
ticks = ticks.astype(int)
ax.set_yticklabels(ticks)

plt.xlabel('Number of Nodes', fontsize=26)
plt.ylabel('Count', fontsize=26)
plt.show(ax)
















































