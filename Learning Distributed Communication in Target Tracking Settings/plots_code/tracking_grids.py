#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 27 15:53:35 2020

@author: destinatoag
"""

import numpy as np
import matplotlib.pyplot as plt 

'''
16.,  7.,  5.,  4.,  4.,  4.,  3.,  3., // 0-8
'''

print('Starting 8 tracking steps grids')

data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,0,1], [1,0,0,1], [0,1,1,0], [1,0,1,1]])
data3 = np.asarray([[1,0,0,1], [1,0,0,1], [0,1,1,0], [1,1,1,1]])
data4 = np.asarray([[1,0,0,1], [1,1,0,1], [0,1,1,0], [1,1,1,1]])
data5 = np.asarray([[1,0,0,1], [1,1,0,1], [0,1,1,0], [1,1,1,1]])
data6 = np.asarray([[1,0,0,1], [1,1,0,1], [0,1,1,0], [1,1,1,1]])
data7 = np.asarray([[1,1,1,1], [1,1,0,0], [0,1,1,0], [1,1,1,1]])
data8 = np.asarray([[1,1,1,1], [1,1,0,0], [0,1,1,0], [1,1,1,1]])


fig, axs = plt.subplots(1, 8, figsize=(12, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)


axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))


plt.show()


#16., 11.,  4.,  4.,  4., 4.,  4.,  4., // 8-16



data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,1,0], [0,1,0,1], [0,0,1,0], [0,0,0,0]])
data3 = np.asarray([[1,1,0,0], [1,1,0,1], [1,0,1,0], [1,1,1,0]])
data4 = np.asarray([[1,1,0,0], [1,1,0,1], [1,0,1,0], [1,1,1,0]])
data5 = np.asarray([[1,1,0,0], [1,1,0,1], [1,0,1,0], [1,1,1,0]])
data6 = np.asarray([[1,1,1,0], [1,1,1,1], [1,0,1,0], [1,1,1,0]])
data7 = np.asarray([[1,1,1,0], [1,1,1,1], [1,0,1,0], [1,1,1,0]])
data8 = np.asarray([[1,1,1,0], [1,1,1,1], [1,0,1,0], [1,1,1,0]])


fig, axs = plt.subplots(1, 8, figsize=(12, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)


axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))

plt.show()



#16.,  9.,  3.,  3.  3.,  3.,  3.,  3., //16-24


data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,0,0], [1,1,1,0], [0,0,1,1], [0,0,1,0]])
data3 = np.asarray([[1,1,1,1], [1,1,1,0], [0,1,1,1], [1,0,1,1]])
data4 = np.asarray([[1,1,1,1], [0,0,1,0], [0,0,1,1], [1,0,1,1]])
data5 = np.asarray([[1,1,1,1], [0,0,1,0], [0,0,1,1], [1,0,1,1]])
data6 = np.asarray([[1,1,1,1], [0,0,1,0], [0,0,1,1], [1,0,1,1]])
data7 = np.asarray([[1,1,1,1], [1,1,1,0], [0,1,1,1], [0,0,1,1]])
data8 = np.asarray([[1,1,1,1], [1,1,1,0], [0,1,1,1], [0,0,1,1]])



fig, axs = plt.subplots(1, 8, figsize=(12, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)


axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))

plt.show()


#16., 14.,  8.,  7.,  7.,  7.,  6.,  6., //24-32



data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,0,0], [0,0,0,0], [0,0,0,0], [0,0,0,1]])
data3 = np.asarray([[1,1,1,0], [0,0,1,0], [0,0,0,0], [1,1,1,1]])
data4 = np.asarray([[1,1,1,0], [0,0,1,1], [0,0,0,0], [1,1,1,1]])
data5 = np.asarray([[1,1,1,0], [0,0,1,1], [0,0,0,0], [1,1,1,1]])
data6 = np.asarray([[1,1,1,0], [0,0,1,1], [0,0,0,0], [1,1,1,1]])
data7 = np.asarray([[1,1,1,1], [0,0,1,1], [0,0,0,1], [1,1,1,1]])
data8 = np.asarray([[1,1,1,1], [0,0,1,1], [0,0,0,1], [1,1,1,1]])

fig, axs = plt.subplots(1, 8, figsize=(12, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)


axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))

plt.show()




#16., 12.,  6.,  6.,  5.,  5.,  5.,  5., //32-40


data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,1,0], [0,0,1,0], [0,0,0,1], [0,0,0,0]])
data3 = np.asarray([[1,1,1,1], [1,0,1,0], [1,1,1,1], [0,0,0,0]])
data4 = np.asarray([[1,1,1,1], [1,0,1,0], [1,1,1,1], [0,0,0,0]])
data5 = np.asarray([[1,1,1,1], [1,1,1,0], [1,1,1,1], [0,0,0,0]])
data6 = np.asarray([[1,1,1,1], [1,1,1,0], [1,1,1,1], [0,0,0,0]])
data7 = np.asarray([[1,1,1,1], [1,1,1,0], [1,1,1,1], [0,0,0,0]])
data8 = np.asarray([[1,1,1,1], [1,1,1,0], [1,1,1,1], [0,0,0,0]])

fig, axs = plt.subplots(1, 8, figsize=(12, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)


axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))

plt.show()


print()
print()
print('Starting a new set of grids on sets of 12!')


data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[0,0,1,1], [0,0,0,1], [0,0,0,0], [0,1,0,1]])
data3 = np.asarray([[0,0,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data4 = np.asarray([[0,0,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data5 = np.asarray([[0,0,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data6 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data7 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data8 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data9 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data10 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data11 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])
data12 = np.asarray([[0,1,1,1], [0,0,0,1], [1,0,0,1], [0,1,0,1]])


fig, axs = plt.subplots(1, 12, figsize=(10, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[8].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[9].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[10].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[11].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)
axs[8].grid(True)
axs[9].grid(True)
axs[10].grid(True)
axs[11].grid(True)



axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))
axs[8].tick_params(axis='x', colors=(0,0,0,0))
axs[8].tick_params(axis='y', colors=(0,0,0,0))
axs[9].tick_params(axis='x', colors=(0,0,0,0))
axs[9].tick_params(axis='y', colors=(0,0,0,0))
axs[10].tick_params(axis='x', colors=(0,0,0,0))
axs[10].tick_params(axis='y', colors=(0,0,0,0))
axs[11].tick_params(axis='x', colors=(0,0,0,0))
axs[11].tick_params(axis='y', colors=(0,0,0,0))

plt.show()


#16., 11.,  4.,  4.,  4., 4.,  4.,  4., // 8-16



data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[0,0,0,0], [0,1,1,1], [0,0,1,0], [0,1,0,0]])
data3 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data4 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data5 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data6 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data7 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data8 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data9 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data10 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [0,1,0,0]])
data11 = np.asarray([[1,1,1,0], [1,0,1,1], [0,0,1,0], [1,1,0,0]])
data12 = np.asarray([[1,1,1,0], [1,0,1,1], [0,1,1,0], [1,1,0,0]])


fig, axs = plt.subplots(1, 12, figsize=(10, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[8].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[9].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[10].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[11].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)
axs[8].grid(True)
axs[9].grid(True)
axs[10].grid(True)
axs[11].grid(True)



axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))
axs[8].tick_params(axis='x', colors=(0,0,0,0))
axs[8].tick_params(axis='y', colors=(0,0,0,0))
axs[9].tick_params(axis='x', colors=(0,0,0,0))
axs[9].tick_params(axis='y', colors=(0,0,0,0))
axs[10].tick_params(axis='x', colors=(0,0,0,0))
axs[10].tick_params(axis='y', colors=(0,0,0,0))
axs[11].tick_params(axis='x', colors=(0,0,0,0))
axs[11].tick_params(axis='y', colors=(0,0,0,0))


plt.show()

#16.,  9.,  3.,  3.  3.,  3.,  3.,  3., //16-24


data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,1,1], [0,0,0,0], [0,0,0,0], [1,0,1,1]])
data3 = np.asarray([[1,0,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data4 = np.asarray([[1,0,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data5 = np.asarray([[1,0,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data6 = np.asarray([[1,0,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data7 = np.asarray([[1,1,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data8 = np.asarray([[1,1,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data9 = np.asarray([[1,1,1,1], [0,1,0,1], [0,0,0,1], [1,0,1,1]])
data10 = np.asarray([[1,1,1,1], [0,0,0,1], [0,0,0,1], [1,1,1,1]])
data11 = np.asarray([[1,1,1,1], [0,0,1,1], [0,0,0,1], [1,1,1,1]])
data12 = np.asarray([[1,1,1,1], [0,0,1,1], [0,0,0,1], [1,1,1,1]])


fig, axs = plt.subplots(1, 12, figsize=(10, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[8].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[9].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[10].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[11].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)
axs[8].grid(True)
axs[9].grid(True)
axs[10].grid(True)
axs[11].grid(True)


axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))
axs[8].tick_params(axis='x', colors=(0,0,0,0))
axs[8].tick_params(axis='y', colors=(0,0,0,0))
axs[9].tick_params(axis='x', colors=(0,0,0,0))
axs[9].tick_params(axis='y', colors=(0,0,0,0))
axs[10].tick_params(axis='x', colors=(0,0,0,0))
axs[10].tick_params(axis='y', colors=(0,0,0,0))
axs[11].tick_params(axis='x', colors=(0,0,0,0))
axs[11].tick_params(axis='y', colors=(0,0,0,0))


plt.show()



#16., 14.,  8.,  7.,  7.,  7.,  6.,  6., //24-32


data1 = np.asarray([[1,1,1,1], [1,1,1,1], [1,1,1,1], [1,1,1,1]])
data2 = np.asarray([[1,0,0,0], [0,1,1,0], [0,0,0,1], [1,1,1,1]])
data3 = np.asarray([[1,0,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data4 = np.asarray([[1,0,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data5 = np.asarray([[1,0,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data6 = np.asarray([[1,0,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data7 = np.asarray([[1,0,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data8 = np.asarray([[1,1,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data9 = np.asarray([[1,1,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data10 = np.asarray([[1,1,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data11 = np.asarray([[1,1,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])
data12 = np.asarray([[1,1,1,1], [0,1,1,0], [0,0,1,1], [1,1,1,1]])


fig, axs = plt.subplots(1, 12, figsize=(10, 6))
axs[0].matshow(data1, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[1].matshow(data2,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[2].matshow(data3,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[3].matshow(data4,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[4].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[5].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[6].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[7].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[8].matshow(data5,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[9].matshow(data6, cmap=plt.cm.summer, extent=[0,4,0,4])
axs[10].matshow(data7,cmap=plt.cm.summer, extent=[0,4,0,4])
axs[11].matshow(data8,cmap=plt.cm.summer, extent=[0,4,0,4])


axs[0].grid(True)
axs[1].grid(True)
axs[2].grid(True)
axs[3].grid(True)
axs[4].grid(True)
axs[5].grid(True)
axs[6].grid(True)
axs[7].grid(True)
axs[8].grid(True)
axs[9].grid(True)
axs[10].grid(True)
axs[11].grid(True)



axs[0].tick_params(axis='x', colors=(0,0,0,0))
axs[0].tick_params(axis='y', colors=(0,0,0,0))
axs[1].tick_params(axis='x', colors=(0,0,0,0))
axs[1].tick_params(axis='y', colors=(0,0,0,0))
axs[2].tick_params(axis='x', colors=(0,0,0,0))
axs[2].tick_params(axis='y', colors=(0,0,0,0))
axs[3].tick_params(axis='x', colors=(0,0,0,0))
axs[3].tick_params(axis='y', colors=(0,0,0,0))
axs[4].tick_params(axis='x', colors=(0,0,0,0))
axs[4].tick_params(axis='y', colors=(0,0,0,0))
axs[5].tick_params(axis='x', colors=(0,0,0,0))
axs[5].tick_params(axis='y', colors=(0,0,0,0))
axs[6].tick_params(axis='x', colors=(0,0,0,0))
axs[6].tick_params(axis='y', colors=(0,0,0,0))
axs[7].tick_params(axis='x', colors=(0,0,0,0))
axs[7].tick_params(axis='y', colors=(0,0,0,0))
axs[8].tick_params(axis='x', colors=(0,0,0,0))
axs[8].tick_params(axis='y', colors=(0,0,0,0))
axs[9].tick_params(axis='x', colors=(0,0,0,0))
axs[9].tick_params(axis='y', colors=(0,0,0,0))
axs[10].tick_params(axis='x', colors=(0,0,0,0))
axs[10].tick_params(axis='y', colors=(0,0,0,0))
axs[11].tick_params(axis='x', colors=(0,0,0,0))
axs[11].tick_params(axis='y', colors=(0,0,0,0))

plt.show()

















